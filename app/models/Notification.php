<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Notification extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	public $timestamps = false;
	protected $table = 'notifications';
	
	
	public static function notificationCreateMyInsurance($data){
		$notification = new Notification;
		   $result = Notification :: where('user_id','=',$data['user_id'])
								->where('insurance_id','=',$data['insurance_id'])
								->where('insurance_type','=','me')->get();
		 $today = date("Y-m-d");
		  
		 $remainingDays = date_diff(date_create($today),date_create($data['renewal_date']));
		  $remainingDays = $remainingDays->format("%a");
		if($data['mode'] == 'Yearly'){
			$noOfDays = 30;
		}
		if($data['mode'] == 'Half Yearly'){
			$noOfDays = 15;
		}
		if($data['mode'] == 'Quarterly'){
			$noOfDays = 15;
		}
		if($data['mode'] == 'Monthly'){
			$noOfDays = 7;
		}
		
		if((count($result) <= 0) ){
			$notification->user_id = $data['user_id'];
			$notification->insurance_id = $data['insurance_id'];
			$notification->renewal_date = $data['renewal_date'];
			$notification->insurance_type = 'me';
			$notification->company_name = Crypt::decrypt($data['company_name']);
			$result = $notification->save();
			
			
		}
		elseif(($remainingDays <= $noOfDays)){
			//push notification
			$data['insurance_type'] = 'me';
			 Notification :: sendInsuranceNotification($data);
			
		}
		
		else{
			
		}
	}
	
		public static function notificationCreateFamilyInsurance($data){
			
		$notification = new Notification;
		 $result = Notification :: where('user_id','=',$data['user_id'])
								->where('insurance_id','=',$data['insurance_id'])
								->where('insurance_type','=','Family')->get();

		$today = date("Y-m-d");
		$remainingDays = date_diff(date_create($today),date_create($data['renewal_date']));
		$remainingDays = $remainingDays->format("%a");
		if($data['mode'] == 'Yearly'){
			$noOfDays = 30;
		}
		if($data['mode'] == 'Half Yearly'){
			$noOfDays = 15;
		}
		if($data['mode'] == 'Quarterly'){
			$noOfDays = 15;
		}
		if($data['mode'] == 'Monthly'){
			$noOfDays = 7;
		}
		
		
		if((count($result) <= 0) ){
			$notification->user_id = $data['user_id'];
		$notification->insurance_id = $data['insurance_id'];
		$notification->renewal_date = $data['renewal_date'];
		$notification->insurance_type = 'Family';
		$notification->company_name = Crypt::decrypt($data['company_name']);
		$result = $notification->save();
			
			
		}

		elseif(($remainingDays <= $noOfDays)){
			//push notification
			$data['insurance_type'] = 'Family';
			 Notification :: sendInsuranceNotification($data);
			
		}
		
		else{
			
		}

	}
		public static function notificationCreateMotorInsurance($data){
		$notification = new Notification;
		$result = Notification :: where('user_id','=',$data['user_id'])
								->where('insurance_id','=',$data['insurance_id'])
								->where('insurance_type','=','Motor')->get();
		$today = date("Y-m-d");
		$remainingDays = date_diff(date_create($today),date_create($data['renewal_date']));
		$remainingDays = $remainingDays->format("%a");
		$noOfDays = 30;
		
		if((count($result) <= 0) ){
			$notification->user_id = $data['user_id'];
			$notification->insurance_id = $data['insurance_id'];
			$notification->insurance_type = 'Motor';
			$notification->renewal_date = $data['renewal_date'];
			$notification->company_name = Crypt::decrypt($data['company_name']);
			$result = $notification->save();
			
			
		}
		elseif(($remainingDays <= $noOfDays)){
			//push notification
			$data['insurance_type'] = 'Motor';
			Notification :: sendInsuranceNotification($data);
			
		}
		
		else{
			
		}
	}
	
	public static function notificationCreateHealthInsurance($data){
		$notification = new Notification;
		$result = Notification :: where('user_id','=',$data['user_id'])
								->where('insurance_id','=',$data['insurance_id'])
								->where('insurance_type','=','Health')->get();
		$today = date("Y-m-d");
		$remainingDays = date_diff(date_create($today),date_create($data['renewal_date']));
		$remainingDays = $remainingDays->format("%a");
		$noOfDays = 30;
		
		

		if((count($result) <= 0) ){
			$notification->user_id = $data['user_id'];
			$notification->insurance_id = $data['insurance_id'];
			$notification->renewal_date = $data['renewal_date'];
			$notification->company_name = Crypt::decrypt($data['company_name']);
			$notification->insurance_type = 'Health';
			$result = $notification->save();
			
			
		}
		elseif(($remainingDays <= $noOfDays)){
			//push notification
			$data['insurance_type'] = 'Health';
			Notification :: sendInsuranceNotification($data);
			
		}
		
		else{
			
		}
	}
	public static function notificationCreateHomeInsurance($data){
		$notification = new Notification;
		$result = Notification :: where('user_id','=',$data['user_id'])
								->where('insurance_id','=',$data['insurance_id'])
								->where('insurance_type','=','Home')->get();
		$today = date("Y-m-d");
		$remainingDays = date_diff(date_create($today),date_create($data['renewal_date']));
		$remainingDays = $remainingDays->format("%a");
		$noOfDays = 30;
		
		


		if((count($result) <= 0) ){
			$notification->user_id = $data['user_id'];
			$notification->insurance_id = $data['insurance_id'];
			$notification->insurance_type = 'Home';
			$notification->renewal_date = $data['renewal_date'];
			$notification->company_name = Crypt::decrypt($data['company_name']);
			$result = $notification->save();
			
			
		}
		elseif(($remainingDays <= $noOfDays)){
			//push notification
			$data['insurance_type'] = 'Home';
			Notification :: sendInsuranceNotification($data);
			
		}
		
		else{
			
		}
	}
	
	public static function notificationCreateTravelInsurance($data){
		$notification = new Notification;
		$result = Notification :: where('user_id','=',$data['user_id'])
								->where('insurance_id','=',$data['insurance_id'])
								->where('insurance_type','=','Travel')->get();
		$today = date("Y-m-d");
		$remainingDays = date_diff(date_create($today),date_create($data['policy_end_date']));
		$remainingDays = $remainingDays->format("%a");
		$noOfDays = 30;
		

		if((count($result) <= 0) ){
			$notification->user_id = $data['user_id'];
			$notification->insurance_id = $data['insurance_id'];
			$notification->insurance_type = 'Travel';
			$notification->renewal_date = $data['end_date'];
			$notification->company_name = Crypt::decrypt($data['company_name']);
			$result = $notification->save();
			
			
		}
		elseif(($remainingDays <= $noOfDays)){
			//push notification
			$data['insurance_type'] = 'Travel';
			 Notification :: sendInsuranceNotification($data);
			
		}
		
		else{
			
		}
		
	}
	public static function getNotification($userId){
		$result = Notification :: where('user_id','=',$userId)
								->orderBy('notification_triggered_on', 'DESC')->get();
		return $result;
	}
	
	public static function sendInsuranceNotification($result){
	
		if($result['gcm_id'] != ''){
				$devices = PushNotification::DeviceCollection(array(
				PushNotification::Device($result['gcm_id'], array('badge' => 1))
				));
				$message = PushNotification::Message('Message Text',array(
							'badge' => 1,
							'insurance_id' => $result['insurance_id'],
							'user_id' => $result['user_id'],
							'company_name' => Crypt::decrypt($result['company_name']),
							'insurance_type' => $result['insurance_type'],
							'sound' => 'theme/notification-tone.aiff',
							'actionLocKey' => 'sds',
							'locKey' => 'localized key',
							'locArgs' => array(
							'localized args',
							'localized args',
							'localized args'
						),
						'launchImage' => 'image.jpg',
						
						'custom' => array('custom data' => array(
							'we' => 'want', 'send to app'
						))
					));
					PushNotification::app('appNameAndroid')
									->to($devices)
									->send($message);
						// Do some crazy things unsuccessfully every minute
						return 'success';
	}
	else return 'failure';
	}
	
	
}