<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class InsuranceLife extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	public $timestamps = false;
	protected $table = 'insurance_life';
	public static $lifeInsuranceRules = array(
											'company_name' => 'required',
											'policy_holder_name' => 'required',
											
											'policy_number' => 'required|numeric|unique:insurance_life,policy_number',
											'policy_name' => 'required',
											'premium' => 'required|numeric',
											'mode' => 'required',
											'premium_payment_term' => 'required|numeric',
											'policy_term' => 'required|numeric',
											'policy_start_date' => 'required',
											'policy_renewal_date' => 'required',
											'sum_assured' => 'required',
											'policy_maturity_date' => 'required',
										);
	public static $lifeInsuranceEditRules = array(
											'company_name' => 'required',
											'policy_holder_name' => 'required',
											'policy_number' => 'required',
											'policy_name' => 'required',
											'premium' => 'required|numeric',
											'mode' => 'required',
											'premium_payment_term' => 'required|numeric',
											'policy_term' => 'required|numeric',
											'policy_start_date' => 'required',
											'policy_renewal_date' => 'required',
											'sum_assured' => 'required',
											'policy_maturity_date' => 'required',
										);	

	public static function decryptResult($result){
		for($i=0;$i<count($result);$i++){
		$response[$i] =  array(
					'id' => $result[$i]['id'],
					'user_id' => $result[$i]['user_id'],
					'belongs_to' => $result[$i]['belongs_to'],
					'company_name' => Crypt::decrypt($result[$i]['company_name']),
					'policy_holder_name' => Crypt::decrypt($result[$i]['policy_holder_name']),
					'policy_number' => Crypt::decrypt($result[$i]['policy_number']),
					'policy_name' => Crypt::decrypt($result[$i]['policy_name']),
					'premium' => Crypt::decrypt($result[$i]['premium']),
					'mode' => $result[$i]['mode'],
					'premium_payment_term' => Crypt::decrypt($result[$i]['premium_payment_term']),
					'renewal_date' => $result[$i]['renewal_date'],
					'policy_term' => Crypt::decrypt($result[$i]['policy_term']),
					'policy_start_date' => Crypt::decrypt($result[$i]['policy_start_date']),
					
					'policy_maturity_date' => Crypt::decrypt($result[$i]['policy_maturity_date']),
					'sum_assured' => Crypt::decrypt($result[$i]['sum_assured']), 
					'status' => $result[$i]['status'],
					'uploaded_on' => $result[$i]['uploaded_on'],
					
					);
				}
		return $response;
	}
	
	public static function createLifeInsurance($data){
		$insurance = InsuranceLife :: where('policy_number','=',$data['policy_number'])->get();
		$noOfInsurance = count($insurance);
			
		if($noOfInsurance > 0){
			//if policy number already exist
			return 409;
		}
		else{
			$lifeInsurance = new InsuranceLife;
			
			
			$startDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_start_date'])));
			
			$renewalDate = date("Y-m-d",strtotime(str_replace("/","-",$data['renewal_date'])));
			
			$policyMaturityDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_maturity_date'])));
			$lifeInsurance->user_id = $data['user_id'];
			$lifeInsurance->belongs_to = $data['belongs_to'];
			$lifeInsurance->company_name = Crypt::encrypt($data['company_name']);
			$lifeInsurance->policy_holder_name = Crypt::encrypt($data['policy_holder_name']);
			$lifeInsurance->policy_number = Crypt::encrypt($data['policy_number']);
			$lifeInsurance->policy_name = Crypt::encrypt($data['policy_name']);
			$lifeInsurance->premium = Crypt::encrypt($data['premium']);
			$lifeInsurance->mode = $data['mode'];
			$lifeInsurance->policy_term = Crypt::encrypt($data['policy_term']);
			$lifeInsurance->policy_start_date = Crypt::encrypt($startDate);
			$lifeInsurance->premium_payment_term = Crypt::encrypt($data['premium_payment_term']);
			$lifeInsurance->renewal_date = $renewalDate;
			$lifeInsurance->policy_maturity_date = Crypt::encrypt($policyMaturityDate);
			$lifeInsurance->sum_assured = Crypt::encrypt($data['sum_assured']);
			$result = $lifeInsurance->save();
			return 1;
		}
	}
	
	public static function editLifeInsurance($data){
		$startDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_start_date'])));
		$renewalDate = date("Y-m-d",strtotime(str_replace("/","-",$data['renewal_date'])));
		$policyMaturityDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_maturity_date'])));
		$result = InsuranceLife :: where('id', '=', $data['id'])
						->update(array('user_id' => $data['user_id'],
										'belongs_to'=> $data['belongs_to'],
										'company_name'=> Crypt::encrypt($data['company_name']),
										'policy_holder_name' => Crypt::encrypt($data['policy_holder_name']),
										'policy_number'=> Crypt::encrypt($data['policy_number']),
										'policy_name'=> Crypt::encrypt($data['policy_name']),
										'premium'=> Crypt::encrypt($data['premium']),
										'mode'=> $data['mode'],
										'policy_term'=> Crypt::encrypt($data['policy_term']),
										'premium_payment_term'=> Crypt::encrypt($data['premium_payment_term']),
										'sum_assured'=> Crypt::encrypt($data['sum_assured']),
										'policy_start_date'=> Crypt::encrypt($startDate),
										'renewal_date'=> $renewalDate,
										'policy_maturity_date'=> Crypt::encrypt($policyMaturityDate),
						));
		return array("status" => "success","response" => "My Insurance Edited successfully");	
	}
	
	public static function myLifeInsurance($userId){
		$insurance = new InsuranceLife;
		$result = InsuranceLife :: where('belongs_to','=','me')
									->where('user_id','=',$userId)->get();
		if(count($result) > 0){
			$response = InsuranceLife :: decryptResult($result);
		}
		else{
			$response = $result;
		}
		
		return $response;
	}
	public static function lifeInsuranceDelete($id){
		$result = InsuranceLife :: where('id', '=', $id)
				->delete();
		return $result;
	}
	
	
	public static function familyLifeInsurance($userId){
		$insurance = new InsuranceLife;
		$result = InsuranceLife :: where('user_id','=',$userId)
									->where('belongs_to','!=','me')->get();
		if(count($result) > 0){
			$response = InsuranceLife :: decryptResult($result);
		}
		else{
			
			$response = $result;
		}
		
		return $response;
	}
	
	public static function lifeInsuranceInDetail($id){
		$insurance = new InsuranceLife;
		$result = InsuranceLife :: where('id','=',$id)->get();
		if(count($result) > 0){
			$response = InsuranceLife :: decryptResult($result);
		}
		else{
			$response = $result;
		}
		
		return $response;
	}
	
	public static function getInsuranceRenewalInfo($belongsTo){
		$today = date("Y-m-d");
		
		$result = InsuranceLife :: join('users', function($join) {
						$join->on('insurance_life.user_id', '=', 'users.id');
					})
					->where('renewal_date','>',$today)
					->where('belongs_to','=',$belongsTo)
					->get();
		//Session::put('notification',$result);
		return $result;
		
	}
	public static function getMyInsuranceRenewalInfo(){
		$today = date("Y-m-d");
		
		$result = InsuranceLife :: join('users', function($join) {
						$join->on('insurance_life.user_id', '=', 'users.id');
					})
					->where('belongs_to','=','me')
					->select(DB::raw('insurance_life.id as insurance_id,users.id as user_id,insurance_life.company_name as company_name,insurance_life.renewal_date as renewal_date,insurance_life.mode as mode,users.gcm_registration_id as gcm_id'))->get();
		
		return $result;
		
	}
	public static function getFamilyInsuranceRenewalInfo(){
		$today = date("Y-m-d");
		
		$result = InsuranceLife :: join('users', function($join) {
						$join->on('insurance_life.user_id', '=', 'users.id');
					})
					
					->where('belongs_to','!=','me')
					->select(DB::raw('insurance_life.id as insurance_id,users.id as user_id,users.gcm_registration_id as gcm_id,insurance_life.mode as mode,insurance_life.company_name as company_name,insurance_life.renewal_date as renewal_date'))->get();
		
		return $result;
		
	}
		public static function getMotorInsuranceRenewalInfo(){
		$today = date("Y-m-d");
		
		$result = InsuranceMotor :: join('users', function($join) {
						$join->on('insurance_motor.user_id', '=', 'users.id');
					})
					
					->select(DB::raw('insurance_motor.id as insurance_id,users.id as user_id,users.gcm_registration_id as gcm_id,insurance_motor.insurance_company_name as company_name,insurance_motor.policy_renewal_date as renewal_date'))->get();
		
		return $result;
		
	}
	public static function getHealthInsuranceRenewalInfo(){
		$today = date("Y-m-d");
		
		$result = InsuranceHealth :: join('users', function($join) {
						$join->on('insurance_health.user_id', '=', 'users.id');
					})
					
					->select(DB::raw('insurance_health.id as insurance_id,users.id as user_id,users.gcm_registration_id as gcm_id,insurance_health.company_name as company_name,insurance_health.policy_renewal_date as renewal_date'))->get();
		
		return $result;
		
	}
	public static function getHomeInsuranceRenewalInfo(){
		$today = date("Y-m-d");
		
		$result = InsuranceHome :: join('users', function($join) {
						$join->on('insurance_home.user_id', '=', 'users.id');
					})
					
					->select(DB::raw('insurance_home.id as insurance_id,users.id as user_id,users.gcm_registration_id as gcm_id,insurance_home.company_name as company_name,insurance_home.policy_renewal_date as renewal_date'))->get();
		
		return $result;
		
	}
	
	public static function getTravelInsuranceRenewalInfo(){
		$today = date("Y-m-d");
		
		$result = InsuranceTravel :: join('users', function($join) {
						$join->on('insurance_travel.user_id', '=', 'users.id');
					})
					
					->select(DB::raw('insurance_travel.id as insurance_id,users.id as user_id,users.gcm_registration_id as gcm_id,insurance_travel.company_name as company_name,insurance_travel.policy_end_date as end_date'))->get();
		
		return $result;
		
	}
	
	
	
	
}