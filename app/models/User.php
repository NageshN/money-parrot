<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	 public $timestamps = false;
	 protected $fillable = ['emailid','password'];
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	public static $rules = array(
								
				'full_name'             => 'required', 						// just a normal required validation
				'phone_number'       => 'numeric|between:1,10',
				'emailid'            => 'required|email|unique:users,email_id', 	// required and must be unique in the ducks table
				'password'         => 'required|min:8|Regex:/^\S+$/',
				're_enter_password' => 'required|same:password', 			// required and has to match the password field
				'terms_and_condition' => 'accepted',
				'phone_number' => 'numeric'
			);
			
			public static $rulesForChangePassword = array(
				
				'password'         => 'required',
				're_enter_password' => 'required|same:password', 			// required and has to match the password field
			);
			public static $rulesForResetMpin = array(
				
				'm_pin'         => 'required',
				're_enter_m_pin' => 'required|same:m_pin', 			// required and has to match the password field
			);
	public static function createUser($type, $data)
    {
		//Check User is already exist or not
			$users = User :: where('email_id','=',$data['email_id'])->get();
			$noOfUsers = count($users);
			
			if($noOfUsers > 0){
				$result = array('status' => "Failure","response" => "EmailId already exist");
			}
			
			else{
				$user = new User;
				$user->full_name = $data['full_name'];
				$user->email_id = $data['email_id'];
				$user->phone_number = $data['phone_number'];
				
				$user->phone_number = $data['phone_number'];
				$user->password = Hash::make($data['password']);
				$user->registered_through = "Self";
				$user->save();
				
				$result = array('status' => "success","response" => "Create user success");
			}
		return ($result);
    }
	public static function validateUser($userId){
		$users = User :: where('id','=',$userId)->get();
		$noOfUsers = count($users);
		if($noOfUsers > 0){
			return 1;
		}
		else{
			return 0;
		}
	}	
	
	public static function registerGCMid($details){
		$users = User :: where('gcm_registration_id','=',$details['gcm_registration_id'])->get();
		$noOfUsers = count($users);
		if($noOfUsers > 0){
			if($details['user_id'] == $users[0]['id']){
				return array("status" => "success","response" => "Gcm Id has registered successfully");	
			}
			else{
				User :: where('id', '=', $users[0]['id'])
						->update(array('gcm_registration_id' => ''));
				User :: where('id', '=', $details['user_id'])
						->update(array('gcm_registration_id' => $details['gcm_registration_id']));
				return array("status" => "success","response" => "Gcm Id has registered successfully");
			}
		}
		else{
			User :: where('id', '=', $details['user_id'])
						->update(array('gcm_registration_id' => $details['gcm_registration_id']));
			return array("status" => "success","response" => "Gcm Id has registered successfully");	
		}
	}
	
	public static function setMpin($details){
		User :: where('id', '=', $details['user_id'])
				->update(array('m_pin' => $details['m_pin']));
		return array("status" => "success","response" => "m pin has registered successfully");	
	}
	public static function validateMpin($details){
		$user = User :: where('id', '=', $details['user_id'])
				->where('m_pin', '=', $details['m_pin'])->get();
		
		if(count($user) > 0){
			return array("status" => "success","response" => "valid mpin for the logged in user");	
		}
		else{
			return array("status" => "failure","response" => "not a valid mpin for the logged in user");	
		}
	}
	public static function resetMpin($details){
		$user = User :: where('id', '=', $details['user_id'])
				->where('email_id', '=', $details['email_id'])->get();
		if(count($user) > 0){
			$result = User :: sendMail($details['email_id'],'resertMpin');
			return $result;
		}
		else{
			return array('status' => "Failure","response" => "Email Id is not matching with User");
		}
	}
	public static function sendMail($emailId,$type){
				$to = $emailId;
				$name = 'Money Parrot';
				$fromEmail = 'hello@moneyparrot.com';
				
				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
				$headers .= 'From: '.$name.'<'.$fromEmail.'>'. "\r\n";
				$subject = "Reset your Mpin";
				$link = "http://codewave.co.in/testbW9uZXkjQCE=/moneyparrot/resetMpin?email=".base64_encode($emailId);
				$message = 'Please find the link below to reset Mpin <a href="'.$link.'" target="_blank">'.$link;
				//$headers .= 'Cc:'. $fromEmail."\r\n";
				$mail = mail($to,$subject,$message,wordwrap($headers));
				if($mail){
					//$forgotPasswordLinkTime = time() + 60 * 60 * 24 * 365; // one year
					$resetMpinLinkTime = time() + 60 * 60; // one hour
					Session::put('resetMpinLinkTime', $resetMpinLinkTime);
					$session = Session :: get('forgotPasswordLinkTime');
					return array('status'=>'success','response'=>'Email has been sent',"session" => $session);
					
				}
				else{
					return  array('status'=>'failure','response'=>'email has not sent');
					
				}
	}
	public static function changeNotificationSettings($details){
		$users = User :: where('id','=',$details['user_id'])->get();
		$noOfUsers = count($users);
			if($details['notification_status'] == true){
				$response = 'on';
			}
			else{
				$response = 'false';
			}
			if($noOfUsers < 0){
				return  array('status' => "Failure","response" => "User is not found");
			}
			else{
				User :: where('id', '=', $details['user_id'])
						->update(array('notification_status' => $details['notification_status']));
				return array("registration" => $response,"response" => "notification setting has been changed");	
			}
		
	}
	public static function createUserFromFacebook($data)
    {
		
		
		$socialUserId = $data['id'];
		
		$user = User::where('social_user_id','=',$socialUserId)->first();
		
		
		if(empty($user)){
			$users = User :: where('email_id','=',$data['email'])->get();
			$noOfUsers = count($users);
			
			if($noOfUsers > 0){
				//email Id already exist
				return 409;
			}
			$user = new User;
			$user->first_name = $data['first_name'];
			$user->last_name = $data['last_name'];
			$user->full_name = $data['name'];
			$user->email_id = $data['email'];
			//$user->password = Hash::make($data['password']);/* 
			$user->registered_through = "Facebook";
			$user->gender = $data['gender'];
			$user->social_user_id = $socialUserId;
		//	$profilePicture = 'https://graph.facebook.com/'.$socialUserId.'/picture?type=large';
		//	$url='https://graph.facebook.com/'.$socialUserId.'/picture?type=large';
		//	$contents=file_get_contents($url);
		//	$save_path="themes/uploads/fbprofile".time().".jpg";
		//	file_put_contents($save_path,$contents);
		//define('DIRECTORY', 'theme/uploads');
		$name = 'theme/uploads/fbprofile'.time().'.jpg';
		$content = file_get_contents('https://graph.facebook.com/'.$socialUserId.'/picture?type=large');
		file_put_contents($name, $content);
			$user->profile_picture = $name; 
			$user->save(); 
			
			return 1;
		}
		
		return 1;
		
    }
	public static function createUserFromGoogle($data)
    {
		$socialUserId = $data['id'];
		
		$user = User::where('social_user_id','=',$socialUserId)->first();
		
		if(empty($user)){
			$users = User :: where('email_id','=',$data['email'])->get();
			$noOfUsers = count($users);
			
			if($noOfUsers > 0){
				//email Id already exist
				return 409;
			}
			$user = new User;
			
			$user->first_name = $data['given_name'];
			$user->last_name = $data['family_name'];
			$user->full_name = $data['name'];
			$user->email_id = $data['email'];
 			$user->gender = $data['gender'];
			
			$user->registered_through = "Google";
			$user->social_user_id = $socialUserId;
			 $user->profile_picture = $data['picture'];
			$user->save(); 
			 
			
			return 1;
		}
		
		return 1;
		
    }
	
	public static function showAllUser(){
		$users = User :: all();
		return $users;
	}
	
	public static function attempt($data,$flag){
		if($flag == true){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static function userLogin($data){
		$hashedPassword = Hash::make($data['password']);
		$users = User :: where('email_id','=',$data['emailId'])->where('password','=',$hashedPassword)->get();
		
		return $noOfUsers = count($users);
		if($noOfUsers > 0){
			return $users;
		}
	}
	public static function resetMpinWeb($data){
		$emailId = $data['email'];
		$mPin = $data['m_pin'];
		
		$user = User :: where('email_id','=',$data['email'])->update(array('m_pin' => $mPin));
		
		return array('status'=>'success');
	}
	public static function changePassword($data){
		$emailId = $data['email'];
		$password = $data['password'];
		$password = Hash::make($password);
		$user = User :: where('email_id','=',$data['email'])->update(array('password' => $password));
		
		return array('status'=>'success');
	}
	
	public static function viewProfile($id){
		$user = User :: where('id','=',$id)->get();
		return $user;
	}
	public static function editProfile($data){
		User :: where('id', '=', $data['user_id'])
											->update(array('phone_number' => $data['phone_number'],'full_name' => $data['full_name'],
											'profile_picture' => $data['imagePath']));
		return 1;
		
	}
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
}
