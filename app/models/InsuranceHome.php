<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class InsuranceHome extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	public $timestamps = false;
	protected $table = 'insurance_home';
	public static $homeRules = array(
								'company_name' => 'required',
								'home_name' => 'required',
								'policy_number' => 'required|unique:insurance_home,policy_number',
								'policy_name' => 'required',
								'policy_term' => 'required',
								'premium' => 'required',
								'policy_start_date' => 'required',
								'policy_renewal_date' => 'required',
								'policy_end_date' => 'required'
								
								
								);
	public static $homeEditRules = array(
								'company_name' => 'required',
								'home_name' => 'required',
								'policy_number' => 'required',
								'policy_name' => 'required',
								'policy_term' => 'required',
								'premium' => 'required',
								'policy_start_date' => 'required',
								'policy_renewal_date' => 'required',
								'policy_end_date' => 'required'
								);
								
	public static function decryptResult($result){
		for($i=0;$i<count($result);$i++){
		$response[$i] =  array('id' => $result[$i]['id'],
					'user_id' => $result[$i]['user_id'],
					'company_name' => Crypt::decrypt($result[$i]['company_name']),
					'home_name' => Crypt::decrypt($result[$i]['home_name']),
					'policy_number' => Crypt::decrypt($result[$i]['policy_number']),
					'policy_name' => Crypt::decrypt($result[$i]['policy_name']),
					'policy_term' => Crypt::decrypt($result[$i]['policy_term']),
					'premium' => Crypt::decrypt($result[$i]['premium']),
					'policy_doc' => Crypt::decrypt($result[$i]['policy_doc']),
					'policy_start_date' => Crypt::decrypt($result[$i]['policy_start_date']),
					'policy_renewal_date' => $result[$i]['policy_renewal_date'], 
					'policy_end_date' => Crypt::decrypt($result[$i]['policy_end_date']),
					'uploaded_on' => $result[$i]['uploaded_on']
					);
				}
		return $response;
	}
	
	public static function homeInsurance($userId){
		$result = InsuranceHome :: where('user_id','=',$userId)->get();
		if(count($result) > 0){
			$response = InsuranceHome :: decryptResult($result);
		}
		else{
			$response = $result;
		}
		
		return $response;
	}
	
	public static function homeInsuranceAdd($data){
		$insurance = InsuranceHome :: where('policy_number','=',$data['policy_number'])->get();
		$noOfInsurance = count($insurance);
			
		if($noOfInsurance > 0){
			//if policy number already exist
			return 409;
		}
		else{
			$startDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_start_date'])));
			$renewalDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_renewal_date'])));
			$endDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_end_date'])));
			
			$homeInsurance = new InsuranceHome;
			$homeInsurance->company_name =  Crypt::encrypt($data['company_name']);
			$homeInsurance->home_name =  Crypt::encrypt($data['home_name']);
			$homeInsurance->policy_doc =  Crypt::encrypt($data['policy_doc']);
			$homeInsurance->user_id =  $data['user_id'];
			$homeInsurance->policy_number =  Crypt::encrypt($data['policy_number']);
			$homeInsurance->policy_name =  Crypt::encrypt($data['policy_name']);
			$homeInsurance->policy_term =  Crypt::encrypt($data['policy_term']);
			$homeInsurance->premium =  Crypt::encrypt($data['premium']);
			$homeInsurance->policy_start_date =  Crypt::encrypt($startDate);
			$homeInsurance->policy_end_date =  Crypt::encrypt($endDate);
			$homeInsurance->policy_renewal_date =  $renewalDate;
			$homeInsurance->save();
			return 1;
			
		}
	}
	
	public static function homeInsuranceView($id){
		//$policyNumber = Crypt::encrypt($policyNumber);
		//eyJpdiI6IklEbmVoZktjM1lROERabVdFcWl4T0E9PSIsInZhbHVlIjoic0ZobGtDamRsbFl5V1RHQmNcL3BzNGc9PSIsIm1hYyI6IjVhOGJmM2U4YjY5ZWUzM2RkMWRjM2E0YTE4NmI2NjliOGM2MmZjYTE4NTUxZjczMWZkNzU5OGZjNmQ2YWRkNjMifQ==
		/* $result = InsuranceHealth :: get()->filter(function($record) use($policyNumber) {
				/* if(Crypt::decrypt($record->policy_number) == $policyNumber) {
					return $record;
				} 
				if($record->id == $policyNumber) {
					return $record;
				} 
		}); */
		$result = InsuranceHome :: where('id','=',$id)->get();
		$noOfInsurance = count($result);
		if($noOfInsurance > 0){
			$response = InsuranceHome :: decryptResult($result);
			return $response;
		}
		else{
			$response = array('status' => 'failure','response' => 'fetch details fails');
			return $response;
		}
	}
	
	public static function homeInsuranceEditPost($data){
			$insurance = InsuranceHome:: where('id', '=', $data['id'])->get();
			
			if($data['policy_doc'] == ''){
				$policyDoc = Crypt::decrypt($insurance[0]['policy_doc']);
			}
			else{
				$policyDoc = $data['policy_doc'];
			}
		
			$startDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_start_date'])));
			$renewalDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_renewal_date'])));
			$endDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_end_date'])));
			$insurance = InsuranceHome :: where('id', '=', $data['id'])
											->update(array('company_name' => Crypt::encrypt($data['company_name']),
											'policy_number' => Crypt::encrypt($data['policy_number']),
											'policy_name' => Crypt::encrypt($data['policy_name']),
											'home_name' => Crypt::encrypt($data['home_name']),
											'policy_doc' => Crypt::encrypt($policyDoc),
											'policy_term' => Crypt::encrypt($data['policy_term']),
											'premium' =>  Crypt::encrypt($data['premium']),
											'policy_start_date' => Crypt::encrypt($startDate),
											'policy_end_date' => Crypt::encrypt($endDate),
											'policy_renewal_date' => $renewalDate,
											));
				
			
	}
	public static function homeInsuranceDelete($id){
		$result = InsuranceHome :: where('id', '=', $id)
				->delete();
		return $result;
	}
	
	}