<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class InsuranceHealth extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	public $timestamps = false;
	protected $table = 'insurance_health';
	public static $healthRules = array(
								'company_name' => 'required',
								'policy_holder_name' => 'required',
								'policy_number' => 'required|unique:insurance_health,policy_number',
								'product_name' => 'required',
								'policy_type' => 'required',
								'premium' => 'required',
								'tenure' => 'required',
								'policy_start_date' => 'required',
								'policy_renewal_date' => 'required',
								'policy_end_date' => 'required',
								'sum_assured' => 'required'
								
								);
	public static $healthEditRules = array(
								'company_name' => 'required',
								'policy_holder_name' => 'required',
								'policy_number' => 'required',
								'product_name' => 'required',
								'policy_type' => 'required',
								'premium' => 'required',
								'tenure' => 'required',
								'policy_start_date' => 'required',
								'policy_renewal_date' => 'required',
								'policy_end_date' => 'required',
								'sum_assured' => 'required');
								
	public static function decryptResult($result){
		for($i=0;$i<count($result);$i++){
		$response[$i] =  array('id' => $result[$i]['id'],
					'company_name' => Crypt::decrypt($result[$i]['company_name']),
					'policy_holder_name' => Crypt::decrypt($result[$i]['policy_holder_name']),
					'policy_number' => Crypt::decrypt($result[$i]['policy_number']),
					'product_name' => Crypt::decrypt($result[$i]['product_name']),
					'premium' => Crypt::decrypt($result[$i]['premium']),
					'policy_doc' => Crypt::decrypt($result[$i]['policy_doc']),
					'tenure' => Crypt::decrypt($result[$i]['tenure']),
					'policy_type' => $result[$i]['policy_type'],
					'policy_start_date' => Crypt::decrypt($result[$i]['policy_start_date']),
					'policy_renewal_date' =>($result[$i]['policy_renewal_date']),
					'policy_end_date' => Crypt::decrypt($result[$i]['policy_end_date']),
					'sum_assured' => Crypt::decrypt($result[$i]['sum_assured']),
					'health_card_image' => Crypt::decrypt($result[$i]['health_card_image']),
					'uploaded_on' => $result[$i]['uploaded_on'],
					
					);
				}
		return $response;
	}
	
	public static function healthInsurance($userId){
		$result = InsuranceHealth :: where('user_id','=',$userId)->get();
		if(count($result) > 0){
			$response = InsuranceHealth :: decryptResult($result);
		}
		else{
			$response = $result;
		}
		
		return $response;
	}
	
	public static function healthInsuranceAdd($data){
		$insurance = InsuranceHealth :: where('policy_number','=',$data['policy_number'])->get();
		$noOfInsurance = count($insurance);
			
		if($noOfInsurance > 0){
			//if policy number already exist
			return 409;
		}
		else{
			$startDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_start_date'])));
			$renewalDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_renewal_date'])));
			$endDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_end_date'])));
			
			$healthInsurance = new InsuranceHealth;
			$healthInsurance->company_name =  Crypt::encrypt($data['company_name']);
			$healthInsurance->policy_holder_name =  Crypt::encrypt($data['policy_holder_name']);
			$healthInsurance->user_id =  $data['user_id'];
			$healthInsurance->policy_number =  Crypt::encrypt($data['policy_number']);
			$healthInsurance->product_name =  Crypt::encrypt($data['product_name']);
			$healthInsurance->policy_type =  $data['policy_type'];
			$healthInsurance->premium =  Crypt::encrypt($data['premium']);
			$healthInsurance->policy_doc =  Crypt::encrypt($data['policy_doc']);	
			$healthInsurance->policy_start_date =  Crypt::encrypt($startDate);
			$healthInsurance->policy_end_date =  Crypt::encrypt($endDate);
			$healthInsurance->tenure =  Crypt::encrypt($data['tenure']);
			$healthInsurance->sum_assured =  Crypt::encrypt($data['sum_assured']);
			$healthInsurance->health_card_image =  Crypt::encrypt($data['health_card_image']);
			$healthInsurance->policy_renewal_date =  ($renewalDate);
			$healthInsurance->save();
			return 1;
			
		}
	}
	
	public static function healthInsuranceView($id){
		//$policyNumber = Crypt::encrypt($policyNumber);
		//eyJpdiI6IklEbmVoZktjM1lROERabVdFcWl4T0E9PSIsInZhbHVlIjoic0ZobGtDamRsbFl5V1RHQmNcL3BzNGc9PSIsIm1hYyI6IjVhOGJmM2U4YjY5ZWUzM2RkMWRjM2E0YTE4NmI2NjliOGM2MmZjYTE4NTUxZjczMWZkNzU5OGZjNmQ2YWRkNjMifQ==
		/* $result = InsuranceHealth :: get()->filter(function($record) use($policyNumber) {
				/* if(Crypt::decrypt($record->policy_number) == $policyNumber) {
					return $record;
				} 
				if($record->id == $policyNumber) {
					return $record;
				} 
		}); */
		$result = InsuranceHealth :: where('id','=',$id)->get();
		$noOfInsurance = count($result);
		if($noOfInsurance > 0){
			$response = InsuranceHealth :: decryptResult($result);
			return $response;
		}
		else{
			$response = array('status' => 'failure','response' => 'fetch details fails');
			return $response;
		}
	}
	
	public static function healthInsuranceEditPost($data){
			$insurance = InsuranceHealth :: where('id', '=', $data['id'])->get();
			if($data['health_card_image'] == ''){
				$healthCard = Crypt::decrypt($insurance[0]['health_card_image']);
			}
			else{
				$healthCard = $data['health_card_image'];
			}
			if($data['policy_doc'] == ''){
				$policyDoc = Crypt::decrypt($insurance[0]['policy_doc']);
			}
			else{
				$policyDoc = $data['policy_doc'];
			}
			$motorInsurance = new InsuranceHealth;
			$startDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_start_date'])));
			$renewalDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_renewal_date'])));
			$endDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_end_date'])));
			$insurance = InsuranceHealth :: where('id', '=', $data['id'])
											->update(array('company_name' => Crypt::encrypt($data['company_name']),
											'policy_number' => Crypt::encrypt($data['policy_number']),
											'product_name' => Crypt::encrypt($data['product_name']),
											'policy_holder_name' => Crypt::encrypt($data['policy_holder_name']),
											'policy_type' => $data['policy_type'],
											'premium' =>  Crypt::encrypt($data['premium']),
											'policy_start_date' => Crypt::encrypt($startDate),
											'policy_end_date' => Crypt::encrypt($endDate),
											'tenure' => Crypt::encrypt($data['tenure']),
											'sum_assured' => Crypt::encrypt($data['sum_assured']),
											'health_card_image' => Crypt::encrypt($healthCard),
											'policy_doc' => Crypt::encrypt($policyDoc),
											
											'policy_renewal_date' => ($renewalDate),
											));
				
			
	}
	public static function healthInsuranceDelete($id){
		$result = InsuranceHealth :: where('id', '=', $id)
				->delete();
		return $result;
	}
	
	}