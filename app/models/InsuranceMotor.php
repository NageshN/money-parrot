<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class InsuranceMotor extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	public $timestamps = false;
	protected $table = 'insurance_motor';
	public static $motorRules = array(
								'vehicle_type' => 'required',
								'make' => 'required',
								'model' => 'required',
								'fuel_type' => 'required',
								'year_of_manufacture' => 'required',
								'insurance_company_name' => 'required',
								'policy_number' => 'required|unique:insurance_motor,policy_number',
								'policy_start_date' => 'required',
								'premium' => 'required',
								'policy_renewal_date' => 'required');
	public static $motorEditRules = array(
								'vehicle_type' => 'required',
								'make' => 'required',
								'model' => 'required',
								'fuel_type' => 'required',
								'year_of_manufacture' => 'required',
								'insurance_company_name' => 'required',
								'policy_number' => 'required',
								'policy_start_date' => 'required',
								'premium' => 'required',
								'policy_renewal_date' => 'required');
								
	public static function motorInsurance($userId){
		$result = InsuranceMotor :: where('user_id','=',$userId)->get();
		if(count($result) > 0){
			$response = InsuranceMotor :: decryptResult($result);
		}
		else{
			$response = $result;
		}
		
		return $response;
	}
	public static function decryptResult($result){
		for($i=0;$i<count($result);$i++){
		$response[$i] =  array(
					'id' => $result[$i]['id'],
					'user_id' => $result[$i]['user_id'],
					'vehicle_type' => $result[$i]['vehicle_type'],
					'make' => Crypt::decrypt($result[$i]['make']),
					'model' => Crypt::decrypt($result[$i]['model']),
					'fuel_type' => Crypt::decrypt($result[$i]['fuel_type']),
					'year_of_manufacture' => Crypt::decrypt($result[$i]['year_of_manufacture']),
					'insurance_company_name' => Crypt::decrypt($result[$i]['insurance_company_name']),
					'policy_number' => Crypt::decrypt($result[$i]['policy_number']),
					'insured_declared_value' =>Crypt::decrypt($result[$i]['insured_declared_value']),
					'policy_doc' =>Crypt::decrypt($result[$i]['policy_doc']),
					'policy_start_date' => Crypt::decrypt($result[$i]['policy_start_date']),
					'policy_renewal_date' => $result[$i]['policy_renewal_date'],
					'premium' => Crypt::decrypt($result[$i]['premium']),
					
					
					'status' => $result[$i]['status'],
					'uploaded_on' => $result[$i]['uploaded_on'],
					
					);
				}
		return $response;
	}
	public static function motorInsuranceAdd($data){
		
		$insurance = InsuranceMotor :: where('policy_number','=',$data['policy_number'])->get();
		$noOfInsurance = count($insurance);
			
		if($noOfInsurance > 0){
			//if policy number already exist
			return 409;
		}
		else{
			$startDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_start_date'])));
			
			$renewalDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_renewal_date'])));
			$motorInsurance = new InsuranceMotor;
			$motorInsurance->vehicle_type = $data['vehicle_type'];
			$motorInsurance->user_id = $data['user_id'];
			$motorInsurance->make = Crypt::encrypt($data['make']);
			$motorInsurance->model = Crypt::encrypt($data['model']);
			$motorInsurance->fuel_type = Crypt::encrypt($data['fuel_type']);
			$motorInsurance->insured_declared_value = Crypt::encrypt($data['insured_declared_value']);
			$motorInsurance->policy_doc = Crypt::encrypt($data['policy_doc']);
			$motorInsurance->year_of_manufacture = Crypt::encrypt($data['year_of_manufacture']);
			$motorInsurance->insurance_company_name = Crypt::encrypt($data['insurance_company_name']);
			$motorInsurance->policy_number = Crypt::encrypt($data['policy_number']);
			$motorInsurance->policy_start_date = Crypt::encrypt($startDate);
			$motorInsurance->premium = Crypt::encrypt($data['premium']);
			$motorInsurance->policy_renewal_date = $renewalDate;
			$motorInsurance->save();
			return 1;
			
		}
	}
	
	public static function motorInsuranceView($id){
		$result = InsuranceMotor :: where('id','=',$id)->get();
		$noOfInsurance = count($result);
		if(count($result) > 0){
			$response = InsuranceMotor :: decryptResult($result);
		}
		else{
			$result = array('status' => 'failure','response' => 'fetch details fails');
			$response = $result;
		}
		
		return $response;
		
		
	}
	
	public static function motorInsuranceEditPost($data){
		$insurance = InsuranceMotor:: where('id', '=', $data['id'])->get();
			
			if($data['policy_doc'] == ''){
				$policyDoc = Crypt::decrypt($insurance[0]['policy_doc']);
			}
			else{
				$policyDoc = $data['policy_doc'];
			}
		$startDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_start_date'])));
			
			$renewalDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_renewal_date'])));
			$motorInsurance = new InsuranceMotor;
			$insurance = InsuranceMotor :: where('id', '=', $data['id'])
											->update(array('vehicle_type' => $data['vehicle_type'],
											'user_id' => $data['user_id'],
											'make' => Crypt::encrypt($data['make']),
											'model' => Crypt::encrypt($data['model']),
											'policy_doc' => Crypt::encrypt($policyDoc),
											'insured_declared_value' => Crypt::encrypt($data['insured_declared_value']),
											'fuel_type' => Crypt::encrypt($data['fuel_type']),
											'year_of_manufacture' => Crypt::encrypt($data['year_of_manufacture']),
											'insurance_company_name' => Crypt::encrypt($data['insurance_company_name']),
											'policy_number' => Crypt::encrypt($data['policy_number']),
											'insurance_company_name' => Crypt::encrypt($data['insurance_company_name']),
											'policy_start_date' => Crypt::encrypt($startDate),
											'policy_renewal_date' => $renewalDate,
											));
				
			
	}
	public static function motorInsuranceDelete($id){
		$result = InsuranceMotor :: where('id', '=', $id)
				->delete();
		return $result;
	}
}