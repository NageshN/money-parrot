<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class InsuranceTravel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	public $timestamps = false;
	protected $table = 'insurance_travel';
	public static $travelRules = array(
								'company_name' => 'required',
								'policy_number' => 'required|unique:insurance_travel,policy_number',
								'policy_name' => 'required',
								'policy_holder_name' => 'required',
								'period' => 'required',
								'premium' => 'required',
								'policy_start_date' => 'required',
								
								'policy_end_date' => 'required'
								
								
								);
	public static $travelEditRules = array(
								'company_name' => 'required',
								'policy_holder_name' => 'required',
								'policy_number' => 'required',
								'policy_name' => 'required',
								'period' => 'required',
								'premium' => 'required',
								'policy_start_date' => 'required',
								
								'policy_end_date' => 'required'
								);
								
	public static function decryptResult($result){
		for($i=0;$i<count($result);$i++){
		$response[$i] =  array('id' => $result[$i]['id'],
					'user_id' => $result[$i]['user_id'],
					'company_name' => Crypt::decrypt($result[$i]['company_name']),
					'policy_holder_name' => Crypt::decrypt($result[$i]['policy_holder_name']),
					'policy_number' => Crypt::decrypt($result[$i]['policy_number']),
					'policy_name' => Crypt::decrypt($result[$i]['policy_name']),
					'period' => Crypt::decrypt($result[$i]['period']),
					'premium' => Crypt::decrypt($result[$i]['premium']),
					'policy_doc' => Crypt::decrypt($result[$i]['policy_doc']),
					'policy_start_date' => Crypt::decrypt($result[$i]['policy_start_date']),
					
					'policy_end_date' => $result[$i]['policy_end_date'],
					'uploaded_on' => $result[$i]['uploaded_on']
					);
				}
		return $response;
	}
	
	public static function travelInsurance($userId){
		
		$result = InsuranceTravel :: where('user_id','=',$userId)->get();
		if(count($result) > 0){
			$response = InsuranceTravel :: decryptResult($result);
		}
		else{
			$response = $result;
		}
		
		return $response;
	}
	
	public static function travelInsuranceAdd($data){
		$insurance = InsuranceTravel :: where('policy_number','=',$data['policy_number'])->get();
		$noOfInsurance = count($insurance);
			
		if($noOfInsurance > 0){
			//if policy number already exist
			return 409;
		}
		else{
			$startDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_start_date'])));
			
			$endDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_end_date'])));
			
			$travelInsurance = new InsuranceTravel;
			$travelInsurance->company_name =  Crypt::encrypt($data['company_name']);
			$travelInsurance->user_id =  $data['user_id'];
			$travelInsurance->policy_number =  Crypt::encrypt($data['policy_number']);
			$travelInsurance->policy_holder_name =  Crypt::encrypt($data['policy_holder_name']);
			$travelInsurance->policy_name =  Crypt::encrypt($data['policy_name']);
			$travelInsurance->period =  Crypt::encrypt($data['period']);
			$travelInsurance->premium =  Crypt::encrypt($data['premium']);
			$travelInsurance->policy_doc =  Crypt::encrypt($data['policy_doc']);
			$travelInsurance->policy_start_date =  Crypt::encrypt($startDate);
			$travelInsurance->policy_end_date =  $endDate;
			
			$travelInsurance->save();
			return 1;
			
		}
	}
	
	public static function travelInsuranceView($id){
		//$policyNumber = Crypt::encrypt($policyNumber);
		//eyJpdiI6IklEbmVoZktjM1lROERabVdFcWl4T0E9PSIsInZhbHVlIjoic0ZobGtDamRsbFl5V1RHQmNcL3BzNGc9PSIsIm1hYyI6IjVhOGJmM2U4YjY5ZWUzM2RkMWRjM2E0YTE4NmI2NjliOGM2MmZjYTE4NTUxZjczMWZkNzU5OGZjNmQ2YWRkNjMifQ==
		/* $result = InsuranceHealth :: get()->filter(function($record) use($policyNumber) {
				/* if(Crypt::decrypt($record->policy_number) == $policyNumber) {
					return $record;
				} 
				if($record->id == $policyNumber) {
					return $record;
				} 
		}); */
		$result = InsuranceTravel :: where('id','=',$id)->get();
		$noOfInsurance = count($result);
		if($noOfInsurance > 0){
			$response = InsuranceTravel :: decryptResult($result);
			return $response;
		}
		else{
			$response = array('status' => 'failure','response' => 'fetch details fails');
			return $response;
		}
	}
	
	public static function travelInsuranceEditPost($data){
		$insurance = InsuranceTravel:: where('id', '=', $data['id'])->get();
			
			if($data['policy_doc'] == ''){
				$policyDoc = Crypt::decrypt($insurance[0]['policy_doc']);
			}
			else{
				$policyDoc = $data['policy_doc'];
			}
		
			$startDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_start_date'])));
			
			$endDate = date("Y-m-d",strtotime(str_replace("/","-",$data['policy_end_date'])));
			$insurance = InsuranceTravel :: where('id', '=', $data['id'])
											->update(array('company_name' => Crypt::encrypt($data['company_name']),
											'policy_number' => Crypt::encrypt($data['policy_number']),
											'policy_holder_name' => Crypt::encrypt($data['policy_holder_name']),
											'policy_name' => Crypt::encrypt($data['policy_name']),
											'period' => Crypt::encrypt($data['period']),
											'premium' =>  Crypt::encrypt($data['premium']),
											'policy_doc' =>  Crypt::encrypt($policyDoc),
											'policy_start_date' => Crypt::encrypt($startDate),
											'policy_end_date' => $endDate,
											
											));
				
			
	}
	public static function travelInsuranceDelete($id){
		$result = InsuranceTravel :: where('id', '=', $id)
				->delete();
		return $result;
	}
	
	}