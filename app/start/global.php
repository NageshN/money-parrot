<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';


Event::listen('cron.collectJobs', function() {
	
	
	
	
	Cron::add('send notification', '0 12 * * *', function() {
		
     
     $myInsurance = InsuranceLife :: getMyInsuranceRenewalInfo();
	   if(count($myInsurance) > 0){
			for($i=0;$i<count($myInsurance);$i++){
					Notification :: notificationCreateMyInsurance($myInsurance[$i]);
			}
			
		}
		
		$familyInsurance = InsuranceLife :: getFamilyInsuranceRenewalInfo();
		if(count($familyInsurance) > 0){
			for($i=0;$i<count($familyInsurance);$i++){
					Notification :: notificationCreateFamilyInsurance($familyInsurance[$i]);
			}
			
		}
		$motorInsurance = InsuranceLife :: getMotorInsuranceRenewalInfo();
		if(count($motorInsurance) > 0){
			for($i=0;$i<count($motorInsurance);$i++){
					Notification :: notificationCreateMotorInsurance($motorInsurance[$i]);
			}
			
		}
		$healthInsurance = InsuranceLife :: getHealthInsuranceRenewalInfo();
		if(count($healthInsurance) > 0){
			for($i=0;$i<count($healthInsurance);$i++){
					Notification :: notificationCreateHealthInsurance($healthInsurance[$i]);
			}
			
		}
		$homeInsurance = InsuranceLife :: getHomeInsuranceRenewalInfo();
		if(count($homeInsurance) > 0){
			for($i=0;$i<count($homeInsurance);$i++){
					Notification :: notificationCreateHomeInsurance($homeInsurance[$i]);
			}
			
		}
		$travelInsurance = InsuranceLife :: getTravelInsuranceRenewalInfo();
		if(count($travelInsurance) > 0){
			for($i=0;$i<count($travelInsurance);$i++){
					Notification :: notificationCreateTravelInsurance($travelInsurance[$i]);
			}
			
		}
		return 'success';
	},true);
	

	
    Cron::add('disabled job', '0 * * * *', function() {
        // Do some crazy things successfully every hour
    }, false);
	$report = Cron::run();
	Cron::setEnablePreventOverlapping();
});

