<?php
	include('header.php');
	$homeInsuranceView = Session::get('homeInsuranceView');
	$companyName = $homeInsuranceView[0]['company_name'];
		$policyNumber = $homeInsuranceView[0]['policy_number'];
		$policyTerm = $homeInsuranceView[0]['policy_term'];
		$homeName = $homeInsuranceView[0]['home_name'];
		$policyName = $homeInsuranceView[0]['policy_name'];
		$premium = $homeInsuranceView[0]['premium'];
		$policyStartDate = $homeInsuranceView[0]['policy_start_date'];
		$policyRenewalDate = $homeInsuranceView[0]['policy_renewal_date'];
		$policyEndDate = $homeInsuranceView[0]['policy_end_date'];
		$policyDoc = $homeInsuranceView[0]['policy_doc'];
		
	
	
	if(Session::has('fieldValue')){
		$value = Session::get('fieldValue');	
		$companyName = $value['company_name'];
		$policyNumber = $value['policy_number'];
		$policyTerm = $value['policy_term'];
		$homeName = $value['home_name'];
		$policyName= $value['policy_name'];
		$premium = $value['premium'];
		$policyStartDate = $value['policy_start_date'];
		$policyRenewalDate = $value['policy_renewal_date'];
		$policyEndDate = $value['policy_end_date'];
		$policyDoc = $value['policy_doc'];
	}
	
	
?>

      
  
      <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-plus"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="#"><i class="fa fa-plus"></i></a></li>
                                    <li><a href="<?=$baseUrl?>generalInsurance">General Insurance</a></li>
									<li><a href="<?=$baseUrl?>generalInsurance/home"><?=$breadcrumbs?></a></li>
									<li>Edit Home Insurance</li>
                                </ul>
                                <h4><?=$breadcrumbs?></h4>
					
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">
						<div class="row">
							<div class="col-md-12">
                                <form class="form-horizontal" method="POST" action =""  enctype="multipart/form-data" files ="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                            </div><!-- panel-btns -->
                                            <h4 class="panel-title">Edit <?=$breadcrumbs?></h4>
                                            <p></p>
                                        </div>
										<div class="panel-body">
										<div class="col-md-6">
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Home Name : </label>
                                                <div class="col-sm-6 <?php if ($errors->has('home_name')) echo 'has-error' ?>">
													<input type="text" name="home_name" class="form-control" value="<?php echo $homeName;?>">
													<?php if ($errors->has('home_name')) ?><p class="help-block"><?=$errors->first('home_name')?></p>
                                                </div>
                                            </div><!-- form-group -->
											
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Company Name : </label>
                                                <div class="col-sm-6 <?php if ($errors->has('company_name')) echo 'has-error' ?>">
													<select type="text" data-placeholder="Company Name" name="company_name" class="chosen-select  form-control" value="<?php echo $companyName;?>" >
														<option value="Apollo DKV Insurance Company Ltd.">Apollo DKV Insurance Company Ltd.</option>
														<option value="Aviva Life Insurance">Aviva Life Insurance</option>
														<option value="Bajaj Allianz General Insurance Co. Ltd.">Bajaj Allianz General Insurance Co. Ltd.</option>
														<option value="Birla Sun Life Insurance">Birla Sun Life Insurance</option>
														<option value="E-Meditek Solutions Limited">E-Meditek Solutions Limited</option>
														<option value="Family Health Plan Limited">Family Health Plan Limited</option>
														<option value="Health India-Bhaichand Amoluk Insurance Services Pvt. Ltd.">Health India-Bhaichand Amoluk Insurance Services Pvt. Ltd.</option>
														<option value="HSBC Health Insurance">HSBC Health Insurance</option>
														<option value="ICICI Lombard General Insurance Co. Ltd.">ICICI Lombard General Insurance Co. Ltd.</option>
														<option value="Life Insurance Corporation Of India">Life Insurance Corporation Of India</option>
														<option value="Max New York Life Insurance">Max New York Life Insurance</option>
														<option value="Med Assist India Ltd.">Med Assist India Ltd.</option>
														<option value="MetLife India Assurance Company">MetLife India Assurance Company</option>
														<option value="National Insurance Company">National Insurance Company</option>
														<option value="Paramound Health Group">Paramound Health Group</option>
														<option value="Reliance Health">Reliance Health</option>
														<option value="Royal Sundaram Alliance Insurance Company Limited">Royal Sundaram Alliance Insurance Company Limited</option>
														<option value="Star Health and Allied Insurance Company Limited">Star Health and Allied Insurance Company Limited</option>
														<option value="Tata AIG">Tata AIG</option>
														<option value="The New India Assurance Co. Ltd."> The New India Assurance Co. Ltd.</option>
														<option value="United Healthcare">United Healthcare</option>
														<option value="United India Insurance">United India Insurance</option>
														
													</select>
													<?php if ($errors->has('company_name')) ?><p class="help-block"><?=$errors->first('company_name')?></p>
                                                </div>
                                            </div><!-- form-group -->
											 <div class="form-group">
                                                <label class="col-sm-4 control-label">Policy Number:</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_number')) echo 'has-error' ?>">
												<input type="text" name="policy_number" class="form-control" value="<?php echo $policyNumber;?>" readonly>
												
												
                                                <?php if ($errors->has('policy_number')) ?><p class="help-block"><?=$errors->first('policy_number')?></p>   
                                                </div>
												
                                            </div><!-- form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Policy Name:</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_name')) echo 'has-error' ?>">
												<input type="text" name="policy_name" class="form-control" value="<?php echo $policyName;?>">
												
												
                                                <?php if ($errors->has('policy_name')) ?><p class="help-block"><?=$errors->first('policy_name')?></p>   
                                                </div>
												
                                            </div><!-- form-group -->
											
											<div class="form-group ">
                                                <label class="col-sm-4 control-label">Policy Term :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_term')) echo 'has-error' ?>">
												
												<input type="text" name="policy_term" class="form-control" value="<?php echo $policyTerm;?>">
                                                  <?php if ($errors->has('policy_term')) ?><p class="help-block"><?=$errors->first('policy_term')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											
											
											
											
											
											
											
											</div>
											<div class="col-md-6">
											<div class="form-group ">
                                                <label class="col-sm-4 control-label">Premium :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('premium')) echo 'has-error' ?>">
												
												<input type="text" name="premium" class="form-control" value="<?php echo $premium;?>">
                                                  <?php if ($errors->has('premium')) ?><p class="help-block"><?=$errors->first('premium')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											
											
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Policy Start Date :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_start_date')) echo 'has-error' ?>">
												<input type="text" name="policy_start_date" id="startDate" class="form-control" value="<?php echo convertDate($policyStartDate);?>">
                                                   <?php if ($errors->has('policy_start_date')) ?><p class="help-block"><?=$errors->first('policy_start_date')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Renewal Date :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_renewal_date')) echo 'has-error' ?>">
												<input type="text" name="policy_renewal_date" id="renewalDate" class="form-control" value="<?php echo convertDate($policyRenewalDate);?>" readonly>
                                                   <?php if ($errors->has('policy_renewal_date')) ?><p class="help-block"><?=$errors->first('policy_renewal_date')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Policy End Date :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_end_date')) echo 'has-error' ?>">
												<input type="text" name="policy_end_date" id="renewalDate" class="form-control" value="<?php echo convertDate($policyEndDate);?>" >
                                                   <?php if ($errors->has('policy_end_date')) ?><p class="help-block"><?=$errors->first('policy_end_date')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Policy Document :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_doc')) echo 'has-error' ?>">
												<input type="file" name="policy_doc" id="renewalDate" class="form-control" value="<?php echo $policyDoc;?>" >
                                                   <?php if ($errors->has('policy_doc')) ?><p class="help-block"><?=$errors->first('policy_doc')?></p>   
                                                </div>
                                            </div><!-- form-group -->
												
											
											
											
											
										</div>	
                                        </div><!-- panel-body -->
                                        <div class="panel-footer" style="text-align:center;">
                                            <button class="btn btn-primary mr5" type="submit" onclick="cilckOnSave();">Save</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div><!-- panel-footer -->
										
                                    </div><!-- panel-default -->
									
									
                                </form>
								
                            </div>
						</div>
                    </div><!-- contentpanel -->
                    
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>
			
		<?php
			include('footer.php');
		?>
		
	
  <script>
  function updateDate(){
		var mode = $('#mode').val();
		var startDate = $('#startDate').val();
		//alert(startDate);
		var formateStartDate = startDate.split('/');
		//alert(formateStartDate[1]);
		var formateStartDate = formateStartDate[1]+'/'+formateStartDate[0]+'/'+formateStartDate[2];
		var startDate = new Date($('#startDate').val());
		$('#startDate').val(formateStartDate);
		
		var noOfdays = 365;
		
		
		var days = parseInt(noOfdays);
		
        if(!isNaN(startDate.getTime())){
            startDate.setDate(startDate.getDate() + days);
            
            $("#renewalDate").val(startDate.toInputFormat());
        } else {
            alert("Invalid Date");  
        }
	}
	
	function updateDateOnChange(){
		var mode = $('#mode').val();
		var startDate = $('#startDate').val();
		//alert(startDate);
		var formateStartDate = startDate.split('/');
		//alert(formateStartDate[1]);
		var formateStartDate = formateStartDate[1]+'/'+formateStartDate[0]+'/'+formateStartDate[2];
		var startDate = new Date(formateStartDate);
		//$('#startDate').val(formateStartDate);
		if(mode == 'Yearly'){
			var noOfdays = 365;
		}
		if(mode == 'Half Yearly'){
			var noOfdays = 182;
		}
		if(mode == 'Quarterly'){
			var noOfdays = 90;
		}
		if(mode == 'Monthly'){
			var noOfdays = 30;
		}
		
		
		var days = parseInt(noOfdays);
		
        if(!isNaN(startDate.getTime())){
            startDate.setDate(startDate.getDate() + days);
            
            $("#renewalDate").val(startDate.toInputFormat());
        } else {
            alert("Invalid Date");  
        }
	}
  
  $(function() {
  
	
    $( "#startDate" ).datepicker({onClose: function() { 
		
		updateDate();
		
	}});
	 $( "#policy_maturity_date" ).datepicker({dateFormat:'dd/mm/yy'});
	
	Date.prototype.toInputFormat = function() {
       var yyyy = this.getFullYear().toString();
       var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = this.getDate().toString();
       return  (dd[1]?dd:"0"+dd[0])+ "/" + (mm[1]?mm:"0"+mm[0]) + "/" + yyyy; // padding
		}
 
  });
  
  </script>


