<?php
	$baseUrl = "http://moneyparrot.com/moneyparrot/";
	$user = Session::get('userInfo');
	$pageName = Session::get('pageName');
	$notification = Session::get('notification');
	
	//$notification = explode(" ",$notification);
	//print_r(json_decode($notification,true));die;
	//$notification = json_decode($notification,true);
	$homeMenu = '';
	$myInsuranceMenu = '';
	$familyInsuranceMenu = '';
	$generalInsuranceMenu = '';
	$mutualFundsMenu = '';
	$otherMenu = '';
	if($pageName == 'home'){
		$homeMenu = 'active';
		$breadcrumbs = 'Home';
	}
	else if($pageName == 'myInsurance'){
		$myInsuranceMenu = 'active';
		$breadcrumbs = 'My Insurance';
	}
	else if($pageName == 'familyInsurance'){
		$familyInsuranceMenu = 'active';
		$breadcrumbs = 'Family Insurance';
	}
	else if($pageName == 'generalInsurance'){
		$generalInsuranceMenu = 'active';
		$breadcrumbs = 'General Insurance';
	}
	else if($pageName == 'motorInsurance'){
		$generalInsuranceMenu = 'active';
		$breadcrumbs = 'Motor Insurance';
	}
	else if($pageName == 'healthInsurance'){
		$generalInsuranceMenu = 'active';
		$breadcrumbs = 'Health Insurance';
	}
	else if($pageName == 'homeInsurance'){
		$generalInsuranceMenu = 'active';
		$breadcrumbs = 'Home Insurance';
	}
	else if($pageName == 'travelInsurance'){
		$generalInsuranceMenu = 'active';
		$breadcrumbs = 'Travel Insurance';
	}
	else if($pageName == 'mutualFunds'){
		$mutualFundsMenu = 'active';
		$breadcrumbs = 'Mutual Funds';
	}
	else if($pageName == 'others'){
		$otherMenu = 'active';
		$breadcrumbs = 'Others';
	}
	
	else if($pageName == 'profile'){
		$breadcrumbs = 'Profile';
	}
	else if($pageName == 'notification'){
		$breadcrumbs = 'Notification';
	}
	
	if(($user['registered_through'] == 'Facebook') || ($user['registered_through'] == 'Google') ){
		$userPhoto = $user['profile_picture'];
	}
	else{
		$userPhoto = $baseUrl.'theme/images/profile.jpg';
		if($user['profile_picture'] != ''){
			$userPhoto = $baseUrl.$user['profile_picture'];
		}
	}

	function convertDate($date){
		return date('d-m-Y',strtotime($date));
	}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<!--<meta http-equiv="refresh" content="10" >-->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Money Parrot - <?=$breadcrumbs?></title>
		<link rel="stylesheet" href="<?=$baseUrl?>theme/choosen/style.css">
		
		<style type="text/css" title="currentStyle">
	
			@import "<?=$baseUrl?>theme/css/demo_page.css";
			@import "<?=$baseUrl?>theme/css/demo_table.css";
			
		</style>
		<style>
				.notification::-webkit-scrollbar
{
  width: 5px;  /* for vertical scrollbars */
  height: 5px; /* for horizontal scrollbars */
}

.notification::-webkit-scrollbar-track
{
  background: rgba(188, 188, 188, 0.5)
}

.notification::-webkit-scrollbar-thumb
{
  background: rgba(0, 0, 0, 0.5);
}
		</style>
  <link rel="stylesheet" href="<?=$baseUrl?>theme/choosen/prism.css">
  <link rel="stylesheet" href="<?=$baseUrl?>theme/choosen/chosen.css">
       
		 <link href="<?=$baseUrl?>theme/css/morris.css" rel="stylesheet">
        <link href="<?=$baseUrl?>theme/css/select2.css" rel="stylesheet" />
		<link href="<?=$baseUrl?>theme/css/style.default.css" rel="stylesheet">
        


        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        
        <header>
            <div class="headerwrapper">
                <div class="header-left">
                    <a href="<?=$baseUrl?>home" class="logo">
                        <img src="<?=$baseUrl?>theme/images/logo-web-1.png" alt="" width="95%"/> 
                    </a>
                    <div class="pull-right">
                        <a href="#" class="menu-collapse">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                </div><!-- header-left -->
                
                <div class="header-right">
                    
                    <div class="pull-right">
                        
                      
                        
                        <div class="btn-group btn-group-list btn-group-notification">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-bell-o"></i>
                              <span class="badge"><?=count($notification);?></span>
                            </button>
                            <div class="dropdown-menu pull-right">
                               
                                <h5>Notification</h5>
                                <ul class="media-list dropdown-list notification" style="max-height: 122px;overflow-y: scroll;">
									<?php
									for($i=0;$i<count($notification);$i++){
									$type = $notification[$i]['insurance_type'];
									if($type == 'me'){
										$type = 'Life';
									}
									
									echo '
                                    <li class="media">
                                        <img class="img-circle pull-left noti-thumb" src="'.$baseUrl.'theme/images/profile.jpg" alt="">
                                        <div class="media-body">
                                          <strong>Your '.$type.' insurance is expiring</strong> 
                                          <small class="date"><i class="fa fa-bullhorn"></i> 2 days Remaining</small>
                                        </div>
                                    </li>';
									}
									?>
                                    
                                </ul>
                                <div class="dropdown-footer text-center">
                                    <a href="<?=$baseUrl?>allNotification" class="link">See All Notifications</a>
                                </div>
                            </div><!-- dropdown-menu -->
                        </div><!-- btn-group -->
                        
                       
                        
                        <div class="btn-group btn-group-option">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                              <li><a href="<?=$baseUrl?>profile"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                             
                              <li><a href="#"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>
                              
                              <li class="divider"></li>
                              <li><a href="<?=$baseUrl?>logout"><i class="glyphicon glyphicon-log-out"></i>Sign Out</a></li>
                            </ul>
                        </div><!-- btn-group -->
                        
                    </div><!-- pull-right -->
                    
                </div><!-- header-right -->
                
            </div><!-- headerwrapper -->
        </header>
        
        <section>
            <div class="mainwrapper">
                <div class="leftpanel">
                    <div class="media profile-left">
                        <a class="pull-left profile-thumb" href="<?=$baseUrl?>profile">
                            <img class="img-circle" src="<?php echo $userPhoto;?>" alt="">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading"><?php echo $user['full_name'];?></h4>
                            <small class="text-muted"></small>
                        </div>
                    </div><!-- media -->
                    
                    <h5 class="leftpanel-title">Menu</h5>
                    <ul class="nav nav-pills nav-stacked">
                        <li class="<?=$homeMenu?>"><a href="<?=$baseUrl?>home"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                        <li class="<?=$myInsuranceMenu?>"><a href="<?=$baseUrl?>myInsurance"><i class="fa fa-heart"></i> <span>My Insurance</span></a></li>
                        <li class="<?=$familyInsuranceMenu?>"><a href="<?=$baseUrl?>familyInsurance"><i class="fa fa-group"></i> <span>Family Insurance</span></a></li>
                        <li class="<?=$generalInsuranceMenu?>"><a href="<?=$baseUrl?>generalInsurance"><i class="fa fa-child"></i> <span>General Insurance</span></a></li>
                        <li class="<?=$mutualFundsMenu?>"><a href="<?=$baseUrl?>mutualFunds"><i class="fa fa-rupee"></i> <span>Mutual Funds</span></a></li>
                        <li class="<?=$otherMenu?>"><a href="<?=$baseUrl?>otherInsurance"><i class="fa fa-suitcase"></i> <span>Others</span></a></li>
                       
                       
                       <!-- <li class="parent"><a href="#"><i class="fa fa-file-text"></i> <span>Pages</span></a>
                            <ul class="children">
                                <li><a href="notfound.html">404 Page</a></li>
                                <li><a href="blank.html">Blank Page</a></li>
                                <li><a href="calendar.html">Calendar</a></li>
                                <li><a href="invoice.html">Invoice</a></li>
                                <li><a href="locked.html">Locked Screen</a></li>
                                <li><a href="media-manager.html">Media Manager</a></li>
                                <li><a href="people-directory.html">People Directory</a></li>
                                <li><a href="profile.html">Profile</a></li>                                
                                <li><a href="search-results.html">Search Results</a></li>
                                <li><a href="signin.html">Sign In</a></li>
                                <li><a href="signup.html">Sign Up</a></li>
                            </ul>
                        </li>-->
                        
                    </ul>
                    
                </div><!-- leftpanel -->