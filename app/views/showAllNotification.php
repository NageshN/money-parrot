<?php
	include('header.php');
	$notification = Session::get('notification');
	
	$today = date("Y-m-d");
?>
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-bell-o"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="#"><i class="fa fa-bell-o"></i></a></li>
                                    <li>All Notification</li>
                                </ul>
                                <h4>All Notification</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel" style="background-color:rgba(247, 247, 247, 0.9);height:100vh;">
						
                       <div class="row">
					   
							<?php
								$content = '';
								$first = false;
								
								for($i=0;$i<count($notification);$i++){
									
									if($notification[$i]['insurance_type'] == 'me'){
										$type="Life";
										$icon = 'fa fa-heart';
										$link = $baseUrl.'lifeInsuranceView/'.$notification[$i]['insurance_id'].'';
									}
									if($notification[$i]['insurance_type'] == 'Family'){
										$type="Family Life";
										$icon = 'fa fa-group';
										$link = $baseUrl.'lifeInsuranceView/'.$notification[$i]['insurance_id'].'';
									}
									if($notification[$i]['insurance_type'] == 'Motor'){
										$type="Motor";
										$icon = 'fa fa-truck';
										$link = $baseUrl.'generalInsurance/motor/view/'.$notification[$i]['insurance_id'].'';
									}
									if($notification[$i]['insurance_type'] == 'Health'){
										$type="Health";
										$icon = 'fa fa-plus';
										$link = $baseUrl.'generalInsurance/health/view/'.$notification[$i]['insurance_id'].'';
									}
									if($notification[$i]['insurance_type'] == 'Home'){
										$type="Home";
										$icon = 'fa fa-home';
										$link = $baseUrl.'generalInsurance/home/view/'.$notification[$i]['insurance_id'].'';
									}
									$remainingDays = date_diff(date_create($today),date_create($notification[$i]['renewal_date']));
									$day = explode(' ',$notification[$i]['notification_triggered_on']);
									if($day[0] == $today){
										$dayName = 'Today';
									}
									else{
										$dayName = date("F j, Y", strtotime($notification[$i]['notification_triggered_on']));
									}
									
									echo '<div class ="col-sm-12"> <div class="day_name">
												'.$dayName.'
											   </div>
											<div class="notification-list">
											<div class="col-sm-1">
												<div class="notification-icon">
												<i class="'.$icon.'"></i>
												</div>
											</div>
											<a href="'.$link.'"><div class="col-sm-11">
												<span class="company_name">'.$notification[$i]['company_name'].'</span><br>
												Your '.$type.' Insurance is about to expire in '.$remainingDays->format("%a").' days 
											</div></a>
										</div></div>';
									
									
									
									}	
									
									
									
								
								
							?>
					   </div>
                        </div>
                    </div><!-- contentpanel -->
                    
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>

		<?php
			include('footer.php');
		?>
		
    </body>
	<script>
	$(document).ready(function() {
		
	}
	function deleteInsurance(e){
			if (confirm("Are you sure?")) {
				  $.ajax({
				  type: "POST",
				  url: "lifeInsuranceDelete/"+e.id+"",
				  datatype: "json",
				  success: function(result){
					if(result.status == 'success'){
						alert(result.response);
						window.location = 'myInsurance';
					}
				  }
				 });
			}
			return false;
			}
			
		setTimeout(function(){ 
			$('#displaySuccess').fadeOut('slow');
		}, 2500);
	</script>
</html>
