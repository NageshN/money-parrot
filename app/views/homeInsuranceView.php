<?php
	include('header.php');
	$homeInsuranceView = Session::get('homeInsuranceView');
	//print_r($homeInsuranceView);
?>
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-plus"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="#"><i class="fa fa-plus"></i></a></li>
									<li><a href="<?=$baseUrl?>generalInsurance">General Insurance</a></li>
                                    <li><a href="<?=$baseUrl?>generalInsurance/home"><?=$breadcrumbs?></a></li>
                                    <li>View <?=$breadcrumbs?></li>
                                </ul>
                                <h4><?=$breadcrumbs?></h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">
						<div class="row">
							<div class="col-md-12">
                              
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                            </div><!-- panel-btns -->
                                            <h4 class="panel-title"> <?=$homeInsuranceView[0]['company_name']?></h4>
                                            
                                        </div>
                                        <div class="panel-body">
										<div class="col-md-6">
											<div class="form-group">
                                                <label class="col-sm-5 control-label">Home Name : </label>
                                                <div class="col-sm-4 ">
													 <?=$homeInsuranceView[0]['home_name']?>
                                                </div>
                                            </div><!-- form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-5 ">Company Name : </label>
                                                <div class="col-sm-4">
                                                   <?=$homeInsuranceView[0]['company_name']?>
                                                </div>
                                            </div><!-- form-group -->
                                        
                                            <div class="form-group">
													<label class="col-sm-5 ">Policy Number :</label>
                                                <div class="col-sm-4">
                                                   <?=$homeInsuranceView[0]['policy_number']?>
                                                </div>
                                            </div><!-- form-group -->
											 <div class="form-group">
                                                <label class="col-sm-5 ">Policy Name :</label>
                                                <div class="col-sm-4">
                                                   <?=$homeInsuranceView[0]['policy_name']?>
                                                </div>
                                            </div><!-- form-group -->
											 <div class="form-group">
                                                <label class="col-sm-5 ">Policy Term :</label>
                                                <div class="col-sm-4">
                                                   <?=$homeInsuranceView[0]['policy_term']?>
                                                </div>
                                            </div><!-- form-group -->
											
											
											
											</div>
											<div class="col-md-6">
											<div class="form-group">
                                                <label class="col-sm-5 ">Premium :</label>
                                                <div class="col-sm-4">
                                                   <?=$homeInsuranceView[0]['premium']?>
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-5 ">Policy Start Date :</label>
                                                <div class="col-sm-4">
                                                   <?=convertDate($homeInsuranceView[0]['policy_start_date']);?>
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-5 ">Renewal Date :</label>
                                                <div class="col-sm-4">
                                                   <?=convertDate($homeInsuranceView[0]['policy_renewal_date']);?>
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-5 ">Policy End Date :</label>
                                                <div class="col-sm-4">
                                                   <?=convertDate($homeInsuranceView[0]['policy_end_date']);?>
                                                </div>
                                            </div><!-- form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-5 ">Policy Documnet :</label>
                                                <div class="col-sm-4">
                                                  <img src="<?=$baseUrl.$homeInsuranceView[0]['policy_doc']?>"  width="35%";>
                                                </div>
                                            </div><!-- form-group -->
											
                                        </div><!-- panel-body -->
											
											</div>
											
                                        <div class="panel-footer" style="text-align:center;">
                                          <a href="<?=$baseUrl?>generalInsurance/home/edit/<?=$homeInsuranceView[0]['id']?>"> <button class="btn btn-primary mr5" > Edit</button></a>
                                            <button class="btn btn-danger" onclick="deleteInsurance(this);" id="<?=$homeInsuranceView[0]['id']?>">Delete</button>
                                        </div><!-- panel-footer -->
                                    </div><!-- panel-default -->
                               
                            </div>
						</div>
                    </div><!-- contentpanel -->
                    
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>
		<?php
			include('footer.php');
		?>
		<script>
			function deleteInsurance(e){
			if (confirm("Are you sure?")) {
				  $.ajax({
				  type: "POST",
				  url: "<?=$baseUrl?>generalInsurance/home/delete/"+e.id+"",
				  datatype: "json",
				  success: function(result){
					if(result.status == 'success'){
						alert(result.response);
						window.location = '<?=$baseUrl?>generalInsurance/home';
					}
				  }
				 });
			}
			return false;
			}
		</script>
    </body>
</html>
