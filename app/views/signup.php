<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Money Parrot</title>

        <link href="theme/css/style.default.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="signin" style="background-image:url('theme/images/background-web.jpg');background-repeat:no-repeat; background-size:cover;">
        
        
        <section>
            
            <div class="panel panel-signup" style="background-color:rgba(0, 0, 0, 0.5);">
                <div class="panel-body">
                    <div class="logo text-center">
                        <img src="theme/images/logo.png" alt="MannyParot Logo" width="38%;">
                    </div>
                    <br />
                    <h4 class="text-center mb5">Create a new account</h4>
           
                    
                    <div class="mb30"></div>
                    
<!-- FORM STARTS HERE -->
<?php	
	
	$phoneNumber = '';
	$fullName = '';
	$emailid = '';
		
	if(Session::has('fieldValue')){
		$value = Session::get('fieldValue');	
		$fullName = $value['full_name'];
		$phoneNumber = $value['phone_number'];
		$emailid = $value['email_id'];
	}

	
?>
<form method="POST" action="signup" novalidate>
                        <div class="row">
                            <div class="col-sm-6 <?php if ($errors->has('full_name')) echo 'has-error' ?>">
                                <div class="input-group mb15 ">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input type="text" class="form-control" placeholder="Enter Full Name" value="<?php echo $fullName;?>" name="full_name">
									
                                </div><!-- input-group -->
								<?php if ($errors->has('full_name')) ?><p class="help-block"><?=$errors->first('full_name')?></p>
                            </div>
                            <div class="col-sm-6 <?php if ($errors->has('emailid')) echo 'has-error' ?>">
                                <div class="input-group mb15">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    <input type="email" class="form-control" placeholder="Enter Email Address" value="<?php echo $emailid;?>" name="emailid">
                                </div><!-- input-group -->
								<?php if ($errors->has('emailid')) ?><p class="help-block"><?=$errors->first('emailid')?></p>
                            </div>
                        </div><!-- row -->
                   
                        <div class="row">
                            
                            <div class="col-sm-6 <?php if ($errors->has('password')) echo 'has-error' ?>">
                                <div class="input-group mb15">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input type="password" class="form-control" placeholder="Enter Password" value="" name="password">
                                </div><!-- input-group -->
								<?php if ($errors->has('password')) ?><p class="help-block"><?=$errors->first('password')?></p>
                            </div>
							<div class="col-sm-6 <?php if ($errors->has('re_enter_password')) echo 'has-error' ?>">
                                <div class="input-group mb15">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input type="password" class="form-control" placeholder="Re Enter Password" value="" name="re_enter_password">
                                </div><!-- input-group -->
								<?php if ($errors->has('re_enter_password')) ?><p class="help-block"><?=$errors->first('re_enter_password')?></p>
                            </div>
                        </div><!-- row -->
						
						<div class="row">
							<div class="col-sm-6 <?php if ($errors->has('phone_number')) echo 'has-error' ?>">
                                <div class="input-group mb15">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    <input type="email" class="form-control" placeholder="Phone Number" value="<?php echo $phoneNumber;?>" name="phone_number">
                                </div><!-- input-group -->
								<?php if ($errors->has('phone_number')) ?><p class="help-block"><?=$errors->first('phone_number')?></p>
                            </div>
                            
						</div>
						
                        <br />
                        <div class="clearfix">
                            <div class="pull-left">
                                <div class="ckbox ckbox-primary mt5 <?php if ($errors->has('terms_and_condition')) echo 'has-error' ?>">
                                    <input type="checkbox" name="terms_and_condition" value="1" id="agree">
                                    <label for="agree" name="terms_and_condition" class="noselect">I agree with <a href="#">Terms and Conditions</a></label>
									<?php if ($errors->has('terms_and_condition')) ?><p class="help-block"><?=$errors->first('terms_and_condition')?>
                                </div>
								
                            </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-primary">Create New Account <i class="fa fa-angle-right ml5"></i></button>
                            </div>
                        </div>
                    </form>
             
                    
                </div><!-- panel-body -->
                <div class="panel-footer">
                    <a  onclick="signIn('<?= url('/login') ?>')"  class="btn btn-success btn-block">Already a Member? Sign In</a>
                </div><!-- panel-footer -->
            </div><!-- panel -->
            
        </section>
		

          <script src="theme/js/jquery-1.11.1.min.js"></script>
        <script src="theme/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="theme/js/bootstrap.min.js"></script>
        <script src="theme/js/modernizr.min.js"></script>
        <script src="theme/js/pace.min.js"></script>
        <script src="theme/js/retina.min.js"></script>
        <script src="theme/js/jquery.cookies.js"></script>

        <script src="theme/js/custom.js"></script>
		<script>
		var signIn = function(name)
		{           
				window.location = name;
		}
		
		
		</script>
    </body>
</html>
