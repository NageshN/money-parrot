<?php
	include('header.php');
	$myLifeDetails = Session::get('myLifeDetails');
	$myLifeSum = Session::get('myLifeSum');
	$familyDetails = Session::get('familyDetails');
	$familyLifeSum = Session::get('familyLifeSum');
	
	$motorDetails = Session::get('motorDetails');
	$healthDetails = Session::get('healthDetails');
	$homeDetails = Session::get('homeDetails');
	$travelDetails = Session::get('travelDetails');

?>
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-home"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="#"><i class="glyphicon glyphicon-home"></i></a></li>
                                    <li>Dashboard</li>
                                </ul>
                                <h4>Dashboard</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel" style="background-color:rgba(247, 247, 247, 0.9);height:100vh;">
                        
                       <div class="row row-stat">
                            <a href="myInsurance"><div class="col-md-4" >
                                <div class="panel panel-white-alt noborder" style="border-top:2px solid #01a499;">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon my-icon"><i class="fa fa-heart"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            <h1 class="mt5" style="float:left;font-size: 24px;">My Insurance</h1>
											<span class="mt5 no-of-insurance fa fa-plus add-insurance my" title="Add New"></span>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">No Of policies </h5>
                                                <h4 class="nomargin"><?=count($myLifeDetails)?></h4>
                                            </div>
                                             <div class="pull-right">
                                                <h5 class="md-title nomargin">Sum Assured</h5>
                                                <h4 class="nomargin">Rs. <?=round($myLifeSum/1000).' K';?></h4>
                                            </div>
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
                            
                           <a href="familyInsurance"><div class="col-md-4">
                                <div class="panel panel-white-alt noborder" style="border-top:2px solid #2c993a;">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon family-icon"><i class="fa fa-group"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            <h1 class="mt5" style="float:left;font-size: 23px;">Family Insurance</h1>
											<span class="mt5 no-of-insurance fa fa-plus family add-insurance" title="Add New"></span>
                                        </div><!-- media-body -->
                                        <hr >
                                        <div class="clearfix mt20">
                                             <div class="pull-left">
                                                <h5 class="md-title nomargin">No Of policies </h5>
                                                <h4 class="nomargin"><?=count($familyDetails)?></h4>
                                            </div>
                                             <div class="pull-right">
                                                <h5 class="md-title nomargin">Sum Assured</h5>
                                                <h4 class="nomargin">Rs. <?=round($familyLifeSum/1000).' K';?></h4>
                                            </div>
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
                            
                            <a href="generalInsurance"><div class="col-md-4">
                                <div class="panel panel-white-alt noborder" style="border-top:2px solid #ca2428;">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon general-icon"><i class="fa fa-child"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            
											 <h1 class="mt5" style="float:left;font-size: 21px;">General Insurance</h1>
											<span class="mt5 no-of-insurance fa fa-plus general add-insurance" title="Add New"></span>
											<ul class="insurance-options" id="general_option">
												<li class="motor"> <span  class="fa fa-truck">  <span class="font-family">Motor</span></span></li>
												<li class="health"><span class="fa fa-plus">  <span class="font-family">Health</span></li>
												<li class="home"><span class="fa fa-home">  <span class="font-family">Home</span></li>
												<li class="travel" style="border:none;"><span class="fa fa-plane">  <span class="font-family">Travel</span></li>
											</ul>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                             <div class="pull-left">
                                                <h5 class="md-title nomargin">No Of policies </h5>
                                                <h4 class="nomargin"><?=count($motorDetails)+count($homeDetails)+count($healthDetails)?></h4>
                                            </div>
                                             
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
                        
                    </div><!-- contentpanel -->
					 <div class="row row-stat">
                            <a href="mutualFunds"><div class="col-md-6">
                                <div class="panel panel-white-alt noborder" style="border-top:2px solid #643c94;">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon mutual-icon "><i class="fa fa-rupee"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            <h1 class="mt5" style="float:left;font-size: 24px;">Mutual Funds</h1>
											<span class="mt5 no-of-insurance fa fa-plus mutual"  title="Add New"></span>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">No Of policies </h5>
                                                <h4 class="nomargin">0</h4>
                                            </div>
                                            
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
							<a href="otherInsurance"><div class="col-md-6">
                                <div class="panel panel-white-alt noborder" style="border-top:2px solid #e19026;">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon others-icon "><i class="fa fa-suitcase"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            <h1 class="mt5" style="float:left;font-size: 24px;">Others</h1>
											<span class="mt5 no-of-insurance fa fa-plus others" title="Add New"></span>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">No Of policies </h5>
                                                <h4 class="nomargin">0</h4>
                                            </div>
                                            
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
						</div>
                    
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>
		<?php
			include('footer.php');
		?>

       <script>
			
			$('.add-insurance').on('click',function(e){
				
				  e.preventDefault();
					if($(this).hasClass('general')){
						$('#general_option').toggle();
						$('.general').toggleClass('general-active');
					}
					if($(this).hasClass('my')){
						window.location = 'lifeInsuranceCreate/me';
					}
					if($(this).hasClass('family')){
						window.location = 'lifeInsuranceCreate/family';
					}
					
				 // window.location = 'lifeInsuranceCreate/me'

			});
			
			$('.motor').on('click',function(e){
				e.preventDefault();
				window.location = "generalInsurance/motor/add";
			});
			$('.health').on('click',function(e){
				e.preventDefault();
				window.location = "generalInsurance/health/add";
			});
			$('.home').on('click',function(e){
				e.preventDefault();
				window.location = "generalInsurance/home/add";
			});
			$('.travel').on('click',function(e){
				e.preventDefault();
				window.location = "generalInsurance/travel/add";
			});
		</script>
    </body>
</html>
