<?php
	include('header.php');
	$lifeInsuranceInDetail = Session::get('lifeInsuranceInDetail');
	if($lifeInsuranceInDetail[0]['belongs_to'] == 'me'){
		$breadCrumbs = "My Insurance";
		$belongsToDisplay = 'display:none;';
		$link = 'myInsurance';
		$icon = 'heart';
	}
	else{
		$breadCrumbs = "Family Insurance";
		$belongsToDisplay = 'display:block;';
		$link = 'familyInsurance';
		$icon = 'group';
	}
	$companyName = trim($lifeInsuranceInDetail[0]['company_name']);
	if((substr($lifeInsuranceInDetail[0]['company_name'], -1) == '.')){
		$companyName = trim($lifeInsuranceInDetail[0]['company_name'],".");
		$companyLogo = $baseUrl."theme/company_logo/".trim($companyName).".jpg";
	}
	else{
		$companyLogo = $baseUrl."theme/company_logo/".trim($companyName).".jpg";
	}
?>
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-<?=$icon?>"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="#"><i class="fa fa-<?=$icon?>"></i></a></li>
                                    <li><a href="<?=$baseUrl.$link?>"><?=$breadCrumbs?></a></li>
                                    <li>View <?=$breadCrumbs?></li>
                                </ul>
                                <h4><?=$breadCrumbs?></h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">
						<div class="row">
							<div class="col-md-12">
                              
                                    <div class="panel panel-default">
										
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                            </div><!-- panel-btns -->
                                           <img src ="<?=$companyLogo?>" class="company_logo"/> <h4 class="panel-title"><?=$lifeInsuranceInDetail[0]['company_name']?></h4>
                                            
                                        </div>
                                        <div class="panel-body">
										<div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-5 ">Company Name : </label>
                                                <div class="col-sm-4">
                                                   <?=$lifeInsuranceInDetail[0]['company_name']?>
                                                </div>
                                            </div><!-- form-group -->
                                        
                                            <div class="form-group">
                                                <label class="col-sm-5 ">Policy Number :</label>
                                                <div class="col-sm-4">
                                                   <?=$lifeInsuranceInDetail[0]['policy_number']?>
                                                </div>
                                            </div><!-- form-group -->
                                             <div class="form-group">
                                                <label class="col-sm-5 ">Policy Holder Name:</label>
                                                <div class="col-sm-4">
                                                   <?=$lifeInsuranceInDetail[0]['policy_holder_name']?>
                                                </div>
                                            </div><!-- form-group -->
											 <div class="form-group">
                                                <label class="col-sm-5 ">Policy Name :</label>
                                                <div class="col-sm-4">
                                                   <?=$lifeInsuranceInDetail[0]['policy_name']?>
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-5 ">Mode :</label>
                                                <div class="col-sm-4">
                                                   <?=$lifeInsuranceInDetail[0]['mode']?>
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-5 ">Premium Payment Term :</label>
                                                <div class="col-sm-4">
                                                   <?=$lifeInsuranceInDetail[0]['premium_payment_term']?>
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group" style="<?=$belongsToDisplay?>">
                                                <label class="col-sm-5 ">Belongs to :</label>
                                                <div class="col-sm-4">
                                                   <?=$lifeInsuranceInDetail[0]['belongs_to']?>
                                                </div>
                                            </div><!-- form-group -->
											
											</div>
											<div class="col-md-6">
											
											
											
											<div class="form-group">
                                                <label class="col-sm-5 ">Policy Term :</label>
                                                <div class="col-sm-4">
                                                   <?=$lifeInsuranceInDetail[0]['policy_term'];?>
                                                </div>
                                            </div><!-- form-group -->
											
											<div class="form-group">
                                                <label class="col-sm-5 ">Policy Start Date :</label>
                                                <div class="col-sm-4">
                                                   <?=convertDate($lifeInsuranceInDetail[0]['policy_start_date']);?>
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-5 ">Renewal Date :</label>
                                                <div class="col-sm-4">
                                                   <?=convertDate($lifeInsuranceInDetail[0]['renewal_date']);?>
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-5 ">Policy maturity date :</label>
                                                <div class="col-sm-4">
                                                   <?=convertDate($lifeInsuranceInDetail[0]['policy_maturity_date']);?>
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-5 ">Sum Assured :</label>
                                                <div class="col-sm-4">
                                                   <?=$lifeInsuranceInDetail[0]['sum_assured']?>
                                                </div>
                                            </div><!-- form-group -->
                                        </div><!-- panel-body -->
										</div>
                                        <div class="panel-footer" style="text-align:center;">
                                          <a href="<?=$baseUrl?>lifeInsuranceEdit/<?=$lifeInsuranceInDetail[0]['id']?>"> <button class="btn btn-primary mr5" > Edit</button></a>
                                            <button class="btn btn-danger" onclick="deleteInsurance(this);" id="<?=$lifeInsuranceInDetail[0]['id']?>">Delete</button>
                                        </div><!-- panel-footer -->
                                    </div><!-- panel-default -->
                               
                            </div>
						</div>
                    </div><!-- contentpanel -->
                    
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>
		<?php
			include('footer.php');
		?>
		<script>
			function deleteInsurance(e){
			if (confirm("Are you sure?")) {
				
				  $.ajax({
				  type: "POST",
				  url: "<?=$baseUrl?>lifeInsuranceDelete/"+e.id+"",
				  datatype: "json",
				  success: function(result){
					
					if(result.status == 'success'){
						alert(result.response);
						
						if(<?php $lifeInsuranceInDetail[0]['belongs_to'] == 'me'?>){
						
							window.location = '<?=$baseUrl?>myInsurance';
						}
						else{
							window.location = '<?=$baseUrl?>familyInsurance';
						}
					}
				  }
				 });
			}
			return false;
			}
		</script>
    </body>
</html>
