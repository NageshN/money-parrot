<?php
	include('header.php');
	$motorDetails = Session::get('motorDetails');
	$healthDetails = Session::get('healthDetails');
	$homeDetails = Session::get('homeDetails');
	$travelDetails = Session::get('travelDetails');

?>
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-child"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="#"><i class="fa fa-child"></i></a></li>
                                    <li><?=$breadcrumbs?></li>
                                </ul>
                                <h4><?=$breadcrumbs?></h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel" style="background-color:rgba(247, 247, 247, 0.9);height:100vh;">
							<div class="row row-stat">
                            <a href="generalInsurance/motor"><div class="col-md-4">
                                <div class="panel panel-white-alt noborder">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon"><i class="fa fa-truck"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            <h1 class="mt5" style="float:left;font-size: 24px;">Motor <br>Insurance</h1>
											<span class="mt5 no-of-insurance fa fa-plus add-insurance motorInsurance" title="Add New"></span>
										
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">No Of policies </h5>
                                                <h4 class="nomargin"><?=count($motorDetails)?></h4>
                                            </div>
                                            
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
                            
                           <a href="generalInsurance/health"><div class="col-md-4">
                                <div class="panel panel-white-alt noborder">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon"><i class="fa fa-plus"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            <h1 class="mt5" style="float:left;font-size: 24px;">Health <br>Insurance</h1>
											<span class="mt5 no-of-insurance fa fa-plus add-insurance healthInsurance" title="Add New"></span>
                                        </div><!-- media-body -->
                                        <hr >
                                        <div class="clearfix mt20">
                                             <div class="pull-left">
                                                <h5 class="md-title nomargin">No Of policies </h5>
                                                <h4 class="nomargin"><?=count($healthDetails)?></h4>
                                            </div>
                                            
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
                            
                            <a href="generalInsurance/home"><div class="col-md-4">
                                <div class="panel panel-white-alt noborder">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon"><i class="fa fa-home"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            
											 <h1 class="mt5" style="float:left;font-size: 24px;">Home <br>Insurance</h1>
											<span class="mt5 no-of-insurance fa fa-plus add-insurance homeInsurance" title="Add New"></span>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                             <div class="pull-left">
                                                <h5 class="md-title nomargin">No Of policies </h5>
                                                <h4 class="nomargin"><?=count($homeDetails)?></h4>
                                            </div>
                                            
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
							<div >
                            <a href="generalInsurance/travel"><div class="col-md-4">
                                <div class="panel panel-white-alt noborder">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon"><i class="fa fa-plane"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            <h1 class="mt5" style="float:left;font-size: 24px;">Travel <br>Insurance</h1>
											<span class="mt5 no-of-insurance fa fa-plus add-insurance travelInsurance" title="Add New"></span>
										
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">No Of policies </h5>
                                                <h4 class="nomargin"><?=count($travelDetails)?></h4>
                                            </div>
                                            
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
							</div>
                    </div><!-- contentpanel -->
                    
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>

		<?php
			include('footer.php');
		?>
		<script>
			function deleteInsurance(e){
			if (confirm("Are you sure?")) {
				  $.ajax({
				  type: "POST",
				  url: "lifeInsuranceDelete/"+e.id+"",
				  datatype: "json",
				  success: function(result){
					if(result.status == 'success'){
						alert(result.response);
						window.location = 'myInsurance';
					}
				  }
				 });
			}
			return false;
			}
			$('.motorInsurance').on('click',function(e){
				e.preventDefault();
				window.location = "generalInsurance/motor/add";
			});
			$('.healthInsurance').on('click',function(e){
				e.preventDefault();
				window.location = "generalInsurance/health/add";
			});
			$('.homeInsurance').on('click',function(e){
				e.preventDefault();
				window.location = "generalInsurance/home/add";
			});
			$('.travelInsurance').on('click',function(e){
				e.preventDefault();
				window.location = "generalInsurance/travel/add";
			});
			
		</script>
    </body>
</html>
