<?php
	include('header.php');
	$user = Session::get('usersDetails');
	$userPhoto = $baseUrl.'theme/images/profile.jpg';
	if($user[0]['profile_picture'] != ''){
		$userPhoto = $user[0]['profile_picture'];
	}
	$myLifeDetails = Session::get('myLifeDetails');
	$myLifeSum = Session::get('myLifeSum');
	$familyDetails = Session::get('familyDetails');
	$familyLifeSum = Session::get('familyLifeSum');
	
	$motorDetails = Session::get('motorDetails');
	$healthDetails = Session::get('healthDetails');
	$homeDetails = Session::get('homeDetails');
	$travelDetails = Session::get('travelDetails');
	$sumAssured = $myLifeSum+$familyLifeSum;
	$totalInsurance = count($myLifeDetails)+count($familyDetails)+count($motorDetails)+count($healthDetails)+count($homeDetails)+count($travelDetails);
	
	$notification = Session::get('notification');
	if(count($notification) > 0){
		$upcomingRenewal = convertDate($notification[0]['renewal_date']);
	}
	else{
		$upcomingRenewal = 'No insurance to renewal';
	}
	
?>
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-user"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="#"><i class="fa fa-user"></i></a></li>
									
                                    <li><?=$breadcrumbs?></li>
                                  
                                </ul>
                                <h4><?=$breadcrumbs?></h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel" style="background-color:rgba(247, 247, 247, 0.9);height:100vh;">
						<div class="row" >
							<div style="float:right;">
								<a href="<?=$baseUrl?>editProfile"><button value="Edit" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</button></a>
							</div>
						</div>
						<div class="row" >
							<div class="col-sm-2 col-md-2">
                                <div class="text-center">
                                    <img src="<?=$userPhoto;?>" class="img-circle img-offline img-responsive img-profile" alt="">
                                   
									<div class="mb20"></div>
                                </div><!-- text-center -->
                            </div>
							<div class="col-sm-4" style="padding-top: 20px;">
								<div class="personal-details">
									<b>Name :</b> <?=$user[0]['full_name'];?>
								</div>
								<div class="personal-details">
									<b>Email Id :</b> <?=$user[0]['email_id'];?>
								</div>
								
								<div class="personal-details">
									<b>Phone Number :</b> <?=$user[0]['phone_number'];?>
								</div>
								
							</div>
							
							<div class="col-md-5" style="padding-top: 20px;" >
								<div class="col-md-4 in-detail">
									Sum	Assured <br>
									<div class="in-detail-value"><?=round($sumAssured/1000).' K';?></div>
								</div>
								<div class="col-md-4 in-detail">
									No of Insurances <br>
									<div class="in-detail-value"><?=$totalInsurance;?></div>
								</div>
								<div class="col-md-4 in-detail">
									Upcoming Renewal <br>
									<div class="in-detail-value"><?=$upcomingRenewal?></div>
								</div>
							</div>
						</div>
						<div class="row">
							<a href="myInsurance"><div class="col-md-3" >
                                <div class="panel panel-white-alt noborder" style="border-top:2px solid #01a499;">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon my-icon small-icon-div"><i class="fa fa-heart"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            <h1 class="mt5" style="float:left;font-size: 15px;">My <br>Insurance</h1>
											
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h6 class="md-title nomargin">No Of policies </h6>
                                                <h4 class="nomargin"><?=count($myLifeDetails)?></h4>
                                            </div>
                                             <div class="pull-right">
                                                <h6 class="md-title nomargin">Sum Assured</h6>
                                                <h4 class="nomargin">Rs. <?=round($myLifeSum/1000).' K';?></h4>
                                            </div>
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
							<a href="familyInsurance"><div class="col-md-3">
                                <div class="panel panel-white-alt noborder" style="border-top:2px solid #2c993a;">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon  small-icon-div family-icon"><i class="fa fa-group"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            <h1 class="mt5" style="float:left;font-size: 15px;">Family <br>Insurance</h1>
											
                                        </div><!-- media-body -->
                                        <hr >
                                        <div class="clearfix mt20">
                                             <div class="pull-left">
                                                <h5 class="md-title nomargin">No Of policies </h5>
                                                <h4 class="nomargin"><?=count($familyDetails)?></h4>
                                            </div>
                                             <div class="pull-right">
                                                <h5 class="md-title nomargin">Sum Assured</h5>
                                                <h4 class="nomargin">Rs. <?=round($familyLifeSum/1000).' K';?></h4>
                                            </div>
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
                            
                            <a href="generalInsurance"><div class="col-md-3">
                                <div class="panel panel-white-alt noborder" style="border-top:2px solid #e41d22;">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon small-icon-div general-icon"><i class="fa fa-child"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            
											 <h1 class="mt5" style="float:left;font-size: 15px;">General <br>Insurance</h1>
											
											
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                             <div class="pull-left">
                                                <h5 class="md-title nomargin">No Of policies </h5>
                                                <h4 class="nomargin"><?=count($motorDetails)+count($homeDetails)+count($healthDetails)?></h4>
                                            </div>
                                             
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
							 <a href="mutualFunds"><div class="col-md-3">
                                <div class="panel panel-white-alt noborder" style="border-top:2px solid #643c94;">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon small-icon-div mutual-icon "><i class="fa fa-rupee"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin"></h5>
                                            <h1 class="mt5" style="float:left;font-size: 15px;">Mutual <br>Funds</h1>
										
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">No Of policies </h5>
                                                <h4 class="nomargin">0</h4>
                                            </div>
                                            
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div></a><!-- col-md-4 -->
						</div>
						
                    
					</div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>
		<?php
			include('footer.php');
		?>
	
    </body>
</html>
