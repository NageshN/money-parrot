<?php
	include('header.php');
	$user = Session::get('userInfo');
	
?>
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-user"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="#"><i class="fa fa-user"></i></a></li>
									
                                    <li><?=$breadcrumbs?></li>
                                  
                                </ul>
                                <h4><?=$breadcrumbs?></h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel" style="background-color:rgba(247, 247, 247, 0.9);height:100vh;">
						<div class="row" >
						<form method="POST" enctype='multipart/form-data' action="">
							<div class="col-sm-4 col-md-3">
                                <div class="text-center">
									
									<input type="hidden" value="<?=$user['profile_picture'];?>" name="profile_image" >
                                    <img id="target"    src="<?=$userPhoto;?>"  class="img-circle img-offline img-responsive img-profile" alt="">
									<input type='file' name="file" id="file" name="Change your image	"/>
                                  
                                   
                                
                                    <div class="mb20"></div>
                                
                                    
                                </div><!-- text-center -->
                                
                                
                              
                                
                              
                            </div>
							<div class="col-sm-5 col-md-5">
							
								<div class="personal-details">
									<b>Name :</b> <input type="text" class="form-control" name="full_name" value="<?=$user['full_name'];?>">
								</div>
								<div class="personal-details">
									<b>Email Id :</b><br> <?=$user['email_id'];?>
								</div>
								
								<div class="personal-details">
									<b>Phone Number :</b> <input type="text"  class="form-control" name="phone_number" value="<?=$user['phone_number'];?>">
								</div>
								
							
								
							</div>
								<div style="clear:both;"><button type="submit" value="save" class="btn btn-info"/> Save </button></div>
							</form>
						</div>
						
                    
					</div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>
		<?php
			include('footer.php');
		?>
	
    </body>
</html>
<script>
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();            
            reader.onload = function (e) {
                $('#target').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#file").change(function(){
        readURL(this);
    });
</script>
