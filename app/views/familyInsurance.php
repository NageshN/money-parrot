<?php
	include('header.php');
	$familyLifeInsurance = Session::get('familyLifeInsurance');
	$rows = count($familyLifeInsurance);
	$message = Session::get('message');
	$displaySuccess = 'display:none	;';
	if($message){
		$displaySuccess = 'display:block;';
	}
?>
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-group"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="#"><i class="fa fa-group"></i></a></li>
                                    <li>Family Insurance</li>
                                </ul>
                                <h4>Family Insurance</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel" style="background-color: rgba(247, 247, 247, 0.9);height: 100vh;">
							
                        <div class="row">
										
										<div class="col-md-12">
											
											<div class="alert alert-info" id="displaySuccess" style="<?=$displaySuccess?>">
												<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
												<strong></strong><?=$message?> 
											</div>
											<div>
										<a href="lifeInsuranceCreate/family"><div class="fa fa-plus-square center btn btn-success mb15" style="float: right;"> &nbsp; <span class="add">Add New<span></div> </a>
										</div>
											<table id="example" class="table table-striped table-bordered dataTable no-footer dtr-inline" style="background-color:#FFF;border-radius:3px !important;border:none;">
												<thead>
													<tr>
													    <th class="center" style="border: none;">No.</th>
														<th class="center" style="border: none;">Company Name</th>
														<th class="center" style="border: none;">Policy name</th>
														<th class="center" style="border: none;">Option</th>
													
													</tr>
												</thead>

							<tbody id="table_body">
								<?php
									for($i=0;$i<$rows;$i++){
										$serialNumber = $i+1;
										echo'<tr>
												<td style="border: none;border-radius:3px">'.$serialNumber.'</td>
												<td style="border: none;font-size: 15px;"><a href="lifeInsuranceView/'.$familyLifeInsurance[$i]['id'].'">'.$familyLifeInsurance[$i]['company_name'].'</a></td>
												<td style="border: none;">'.$familyLifeInsurance[$i]['policy_name'].'</td>
												<td style="border: none;border-radius:3px"><a href="lifeInsuranceView/'.$familyLifeInsurance[$i]['id'].'" title="View" class="fa fa-eye center btn btn-info" style="margin-right:10px"><i></i></a>
												<a href="lifeInsuranceEdit/'.$familyLifeInsurance[$i]['id'].'" title="Edit" class="fa fa-pencil center btn btn-success" style="margin-right:10px"><i></i></a>
												<a href="#" title="Delete" id="'.$familyLifeInsurance[$i]['id'].'" onclick = "deleteInsurance(this);" class="fa fa-trash-o center btn btn-danger" style="margin-right:10px"><i></i></a></td>
											</tr>';
									}
								?>
						</tbody>
											</table>	
 
									</div>
									
								</div>
                       
                        
                    </div><!-- contentpanel -->
                    
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>

		<?php
			include('footer.php');
		?>
		<script>
			function deleteInsurance(e){
			if (confirm("Are you sure?")) {
				  $.ajax({
				  type: "POST",
				  url: "lifeInsuranceDelete/"+e.id+"",
				  datatype: "json",
				  success: function(result){
					if(result.status == 'success'){
						alert(result.response);
						window.location = 'familyInsurance';
					}
				  }
				 });
			}
			return false;
			}
			
		setTimeout(function(){ 
			$('#displaySuccess').fadeOut('slow');
		}, 2500);
	
		</script>
    </body>
</html>
