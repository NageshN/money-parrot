<?php
	include('header.php');
	$vehicleType = '';
	$yearOfManufacture = '';
	$policyCompanyName = '';
	$policyNumber = '';
	$policyStartDate = '';
	$policyRenewalDate = '';
	$premium = '';
	$make = '';
	$model = '';
	$fueltype = '';
	$policyDoc = '';
	$insuredDeclaredValue = '';
	
	
	if(Session::has('fieldValue')){
		$value = Session::get('fieldValue');	
		$vehicleType = $value['vehicle_type'];
		$yearOfManufacture = $value['year_of_manufacture'];
		$policyCompanyName = $value['insurance_company_name'];
		$policyNumber = $value['policy_number'];
		$premium = $value['premium'];
		$policyStartDate = $value['policy_start_date'];
		$policyRenewalDate = $value['policy_renewal_date'];
		$make = $value['make'];
		$model = $value['model'];
		$fueltype = $value['fueltype'];
		$policyDoc = $value['policy_doc'];
		$insuredDeclaredValue = $value['insured_declared_value'];
		
	}
	
	
?>

      
  
      <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-heart"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                    <li><a href="<?=$baseUrl?>generalInsurance">General Insurance</a></li>
									<li><a href="<?=$baseUrl?>generalInsurance/motor"><?=$breadcrumbs?></a></li>
									<li>Add Motor Insurance</li>
                                </ul>
                                <h4><?=$breadcrumbs?></h4>
					
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">
						<div class="row">
							<div class="col-md-12">
                                <form class="form-horizontal" method="POST" action ="" enctype="multipart/form-data" files ="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                            </div><!-- panel-btns -->
                                            <h4 class="panel-title">Create <?=$breadcrumbs?></h4>
                                            <p></p>
                                        </div>
										<div class="panel-body">
										<div class="col-md-6">
                                        
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Vehicle Type : </label>
                                                <div class="col-sm-6 <?php if ($errors->has('vehicle_type')) echo 'has-error' ?>">
													<select type="text" data-placeholder="Vehicle Type" name="vehicle_type" class="form-control" value="<?php echo $vehicleType;?>"  value="">
														<option value="two_wheeler">Two Wheeler</option>
														<option value="four_wheeler">Four Wheeler</option>
														
													</select>
													<?php if ($errors->has('vehicle_type')) ?><p class="help-block"><?=$errors->first('vehicle_type')?></p>
                                                </div>
                                            </div><!-- form-group -->
											 <div class="form-group">
                                                <label class="col-sm-4 control-label">Make:</label>
                                                <div class="col-sm-6 <?php if ($errors->has('make')) echo 'has-error' ?>">
												<select type="text" data-placeholder="make" name="make" class="chosen-select col-sm-6 form-control" value="<?php echo $make;?>"  value="">
													<option value="Ashok Leyland">Ashok Leyland</option>
	<option value="Aston Martin">Aston Martin</option>
	<option value="Audi">Audi</option>
	<option value="Bentley">Bentley</option>
	<option value="Bentley">Bentley</option>
	<option value="Bugatti">Bugatti</option>
	<option value="Caterham">Caterham</option>
	<option value="Chevrolet">Chevrolet</option>
	<option value="Datsun">Datsun</option>
	<option value="Ferrari">Ferrari</option>
	<option value="Ferrari">Ferrari</option>
	<option value="Force Motors">Force Motors</option>
	<option value="Ford">Ford</option>
	<option value="Honda">Honda</option>
	<option value="Hyundai">Hyundai</option>
	<option value="ICML">ICML</option>
	<option value="Isuzu">Isuzu</option>
	<option value="Jaguar">Jaguar</option>
	<option value="Lamborghini">Lamborghini</option>
	<option value="Land Rover">Land Rover</option>
	<option value="Mahindra">Mahindra</option>
	<option value="Maruti Suzuki">Maruti Suzuki</option>
	<option value="Maserati">Maserati</option>
	<option value="Mercedes-Benz">Mercedes-Benz</option>
	<option value="Mini">Mini</option>
	<option value="Mitsubishi">Mitsubishi</option>
	<option value="Nissan">Nissan</option>
	<option value="Porsche">Porsche</option>
	<option value="Premier">Premier</option>
	<option value="Renault">Renault</option>
	<option value="Rolls-Royce">Rolls-Royce</option>
	<option value="Skoda">Skoda</option>
	<option value="Ssangyong">Ssangyong</option>
	<option value="Tata">Tata</option>
	<option value="Toyota">Toyota</option>
	<option value="Volkswagen">Volkswagen</option>
	<option value="Volvo">Volvo</option>

												</select>
                             					<?php if ($errors->has('make')) ?><p class="help-block"><?=$errors->first('make')?></p>   
                                                </div>
												
                                            </div><!-- form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Manufacture Year:</label>
                                                <div class="col-sm-6 <?php if ($errors->has('year_of_manufacture')) echo 'has-error' ?>">
												<input type="text" name="year_of_manufacture" class="form-control" value="<?php echo $yearOfManufacture;?>">
												
												
                                                <?php if ($errors->has('year_of_manufacture')) ?><p class="help-block"><?=$errors->first('year_of_manufacture')?></p>   
                                                </div>
												
                                            </div><!-- form-group -->
                                             <div class="form-group">
                                                <label class="col-sm-4 control-label">Model : </label>
                                                <div class="col-sm-6 <?php if ($errors->has('model')) echo 'has-error' ?>">
												<input type="text" name="model" class="form-control" value="<?php echo $model;?>">
												
												
                                                <?php if ($errors->has('model')) ?><p class="help-block"><?=$errors->first('model')?></p>   
                                                </div>
												
                                            </div><!-- form-group -->
											 <div class="form-group">
                                                <label class="col-sm-4 control-label">Insurance Company Name :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('insurance_company_name')) echo 'has-error' ?>">
												
												<input type="text" name="insurance_company_name" class="form-control" value="<?php echo $policyCompanyName;?>">
                                                  <?php if ($errors->has('insurance_company_name')) ?><p class="help-block"><?=$errors->first('insurance_company_name')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											 <div class="form-group">
                                                <label class="col-sm-4 control-label">Insured Declared Value :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('insured_declared_value')) echo 'has-error' ?>">
												
												<input type="text" name="insured_declared_value" class="form-control" value="<?php echo $insuredDeclaredValue;?>">
                                                  <?php if ($errors->has('insured_declared_value')) ?><p class="help-block"><?=$errors->first('insuredDeclaredValue')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											
											
											
											
											</div>
											<div class="col-md-6">
												<div class="form-group" style="">
                                                <label class="col-sm-4 control-label">Fuel Type :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('fuel_type')) echo 'has-error' ?>">
												
												<select  name="fuel_type" class="form-control" value="<?php echo $fueltype;?>">
													<option value="Petrol">Petrol</option>
													<option value="Petrol">Diesel</option>
													<option value="Petrol">CNG</option>
													<option value="Petrol">Electric</option>

												</select>
                                                  <?php if ($errors->has('fuel_type')) ?><p class="help-block"><?=$errors->first('fuel_type')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group" style="">
                                                <label class="col-sm-4 control-label">Policy Number :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_number')) echo 'has-error' ?>">
												
												<input type="text" name="policy_number" class="form-control" value="<?php echo $policyNumber;?>">
                                                  <?php if ($errors->has('policy_number')) ?><p class="help-block"><?=$errors->first('policy_number')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Policy Start Date :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_start_date')) echo 'has-error' ?>">
												<input type="text" name="policy_start_date" id="startDate" class="form-control" value="<?php echo $policyStartDate;?>">
                                                   <?php if ($errors->has('policy_start_date')) ?><p class="help-block"><?=$errors->first('policy_start_date')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Renewal Date :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_renewal_date')) echo 'has-error' ?>">
												<input type="text" name="policy_renewal_date" id="renewalDate" class="form-control" value="<?php echo $policyRenewalDate;?>" readonly>
                                                   <?php if ($errors->has('policy_renewal_date')) ?><p class="help-block"><?=$errors->first('policy_renewal_date')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group ">
                                                <label class="col-sm-4 control-label">Policy Document :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_doc')) echo 'has-error' ?>">
												
												<input type="file" name="policy_doc" class="form-control" value="<?php echo $policyDoc;?>">
                                                  <?php if ($errors->has('policy_doc')) ?><p class="help-block"><?=$errors->first('policy_doc')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group ">
                                                <label class="col-sm-4 control-label">Premium :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('premium')) echo 'has-error' ?>">
												
												<input type="text" name="premium" class="form-control" value="<?php echo $premium;?>">
                                                  <?php if ($errors->has('premium')) ?><p class="help-block"><?=$errors->first('premium')?></p>   
                                                </div>
                                            </div><!-- form-group -->
                                            
										</div>	
                                        </div><!-- panel-body -->
                                        <div class="panel-footer" style="text-align:center;">
                                            <button class="btn btn-primary mr5" type="submit" onclick="cilckOnSave();">Save</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div><!-- panel-footer -->
										
                                    </div><!-- panel-default -->
									
									
                                </form>
								
                            </div>
						</div>
                    </div><!-- contentpanel -->
                    
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>
			
		<?php
			include('footer.php');
		?>
		
	
  <script>
  function updateDate(){
		var mode = $('#mode').val();
		var startDate = $('#startDate').val();
		//alert(startDate);
		var formateStartDate = startDate.split('/');
		//alert(formateStartDate[1]);
		var formateStartDate = formateStartDate[1]+'/'+formateStartDate[0]+'/'+formateStartDate[2];
		var startDate = new Date($('#startDate').val());
		$('#startDate').val(formateStartDate);
		
		var noOfdays = 365;
		
		
		var days = parseInt(noOfdays);
		
        if(!isNaN(startDate.getTime())){
            startDate.setDate(startDate.getDate() + days);
            
            $("#renewalDate").val(startDate.toInputFormat());
        } else {
            alert("Invalid Date");  
        }
	}
	
	function updateDateOnChange(){
		var mode = $('#mode').val();
		var startDate = $('#startDate').val();
		//alert(startDate);
		var formateStartDate = startDate.split('/');
		//alert(formateStartDate[1]);
		var formateStartDate = formateStartDate[1]+'/'+formateStartDate[0]+'/'+formateStartDate[2];
		var startDate = new Date(formateStartDate);
		//$('#startDate').val(formateStartDate);
		if(mode == 'Yearly'){
			var noOfdays = 365;
		}
		if(mode == 'Half Yearly'){
			var noOfdays = 182;
		}
		if(mode == 'Quarterly'){
			var noOfdays = 90;
		}
		if(mode == 'Monthly'){
			var noOfdays = 30;
		}
		
		
		var days = parseInt(noOfdays);
		
        if(!isNaN(startDate.getTime())){
            startDate.setDate(startDate.getDate() + days);
            
            $("#renewalDate").val(startDate.toInputFormat());
        } else {
            alert("Invalid Date");  
        }
	}
  
  $(function() {
  
	
    $( "#startDate" ).datepicker({onClose: function() { 
		
		updateDate();
		
	}});
	 $( "#policy_maturity_date" ).datepicker({dateFormat:'dd/mm/yy'});
	
	Date.prototype.toInputFormat = function() {
       var yyyy = this.getFullYear().toString();
       var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = this.getDate().toString();
       return  (dd[1]?dd:"0"+dd[0])+ "/" + (mm[1]?mm:"0"+mm[0]) + "/" + yyyy; // padding
		}
 
  });
  
  </script>


