<?php
	include('header.php');
	$myLifeInsuranceInDetail = Session::get('myLifeInsuranceInDetail');
	$companyName = '';
	$policyNumber = '';
	$policyHolderName = '';
	$policyName = '';
	$premium = '';
	$premiumPaymentTerm = '';
	$policeTerm = '';
	$mode = '';
	$policyStartDate = '';
	$policyRenewalDate = '';
	$sumAssured = '';
	$policyMaturityDate = '';
	
	if(Session::has('fieldValue')){
		$value = Session::get('fieldValue');	
		$companyName = $value['company_name'];
		$policyHolderName = $value['policy_holder_name'];
		$policyNumber = $value['policy_number'];
		$policyName = $value['policy_name'];
		$premium = $value['premium'];
		$mode = $value['mode'];
		$premiumPaymentTerm = $value['premium_payment_term'];
		$policeTerm = $value['policy_term'];
		$policyStartDate = $value['policy_start_date'];
		$policyRenewalDate = $value['renewal_date'];
		$sumAssured = $value['sum_assured'];
		
	}
	if($myLifeInsuranceInDetail[0]['belongs_to'] == 'me'){
		$breadCrumbs = "My Insurance";
		$belongsToDisplay = 'display:none;';
		$link = 'myInsurance';
			$icon = 'heart';
	}
	else{
		$breadCrumbs = "Family Insurance";
		$belongsToDisplay = 'display:block;';
		$link = 'familyInsurance';
			$icon = 'group';
	}
	//print_r($myLifeInsuranceInDetail[0]['company_name']); die;
?>
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-<?=$icon?>"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="#"><i class="fa fa-<?=$icon?>"></i></a></li>
                                    <li><a href="<?=$baseUrl.$link?>"><?=$breadCrumbs?></a></li>
                                    <li>Edit <?=$breadCrumbs?></li>
                                </ul>
                                <h4><?=$breadCrumbs?></h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">
						<div class="row">
							<div class="col-md-12">
                                <form class="form-horizontal" action="" method="POST">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-btns" style="display: none;">
                                                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                            </div><!-- panel-btns -->
                                            <h4 class="panel-title"><?=$myLifeInsuranceInDetail[0]['company_name']?></h4>

                                        </div>
                                        <div class="panel-body">
										<div class="col-md-6">
                                        
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Company Name : </label>
                                                <div class="col-sm-6 <?php if ($errors->has('full_name')) echo 'has-error' ?>">
												<select type="text" data-placeholder="Company Name" name="company_name" class="chosen-select col-sm-6 form-control" value="<?=$myLifeInsuranceInDetail[0]['company_name']?>"  value="">
													
														<option value="Bajaj Allianz Life Insurance Company Limited ">Bajaj Allianz Life Insurance Company Limited </option>
														<option value="Birla Sun Life Insurance Co. Ltd ">Birla Sun Life Insurance Co. Ltd </option>
														<option value="HDFC Standard Life Insurance Co. Ltd ">HDFC Standard Life Insurance Co. Ltd </option>
														<option value="ICICI Prudential Life Insurance Co. Ltd ">ICICI Prudential Life Insurance Co. Ltd </option>
														<option value="Exide Life Insurance Company Limited">Exide Life Insurance Company Limited</option>
														<option value="Life Insurance Corporation of India">Life Insurance Corporation of India</option>
														<option value="Max Life Insurance Co. Ltd ">Max Life Insurance Co. Ltd ax Life Insurance Co. Ltd </option>
														<option value="PNB Metlife India Insurance Co. Ltd.">PNB Metlife India Insurance Co. Ltd.</option>
														<option value="Kotak Mahindra Old Mutual Life Insurance Limited">Kotak Mahindra Old Mutual Life Insurance Limited</option>
														<option value="SBI Life Insurance Co. Ltd ">SBI Life Insurance Co. Ltd </option>
														<option value="Tata AIA Life Insurance Company Limited">Tata AIA Life Insurance Company Limited</option>
														<option value="Reliance Life Insurance Company Limited.">Reliance Life Insurance Company Limited.</option>
														<option value="Aviva Life Insurance Company India Limited">Aviva Life Insurance Company India Limited</option>
														<option value="Sahara India Life Insurance Co, Ltd. ">Sahara India Life Insurance Co, Ltd. </option>
														<option value="Shriram Life Insurance Co, Ltd. ">Shriram Life Insurance Co, Ltd. </option>
														<option value="Bharti AXA Life Insurance Company Ltd. ">Bharti AXA Life Insurance Company Ltd. </option>
														<option value="Future Generali India Life Insurance Company Limited ">Future Generali India Life Insurance Company Limited </option>
														<option value="IDBI Federal Life Insurance Company Ltd.">IDBI Federal Life Insurance Company Ltd.</option>
														<option value="Canara HSBC Oriental Bank of Commerce Life Insurance Company Ltd. ">Canara HSBC Oriental Bank of Commerce Life Insurance Company Ltd. </option>
														<option value="AEGON Religare Life Insurance Company Limited. ">AEGON Religare Life Insurance Company Limited. </option>
														<option value="DHFL Pramerica Life Insurance Co. Ltd.">DHFL Pramerica Life Insurance Co. Ltd.</option>
														<option value="Star Union Dai-ichi Life Insurance Co. Ltd.">Star Union Dai-ichi Life Insurance Co. Ltd.</option>
														<option value="IndiaFirst Life Insurance Company Limited">IndiaFirst Life Insurance Company Limited</option>
														<option value="Edelweiss Tokio Life Insurance Co. Ltd.">Edelweiss Tokio Life Insurance Co. Ltd.</option>
														
													</select>
													<?php if ($errors->has('full_name')) ?><p class="help-block"><?=$errors->first('full_name')?></p>
                                                </div>
                                            </div><!-- form-group -->
                                        
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Policy Number :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_number')) echo 'has-error' ?>">
												<input type="text" name="policy_number" class="form-control" value="<?=$myLifeInsuranceInDetail[0]['policy_number']?>" readonly>
												
												<input type="hidden" name="belongs_to" class="form-control" value="me">
                                                <?php if ($errors->has('policy_number')) ?><p class="help-block"><?=$errors->first('policy_number')?></p>   
                                                </div>
												
                                            </div><!-- form-group -->
											 <div class="form-group">
                                                <label class="col-sm-4 control-label">Policy Name :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_name')) echo 'has-error' ?>">
												
												<input type="text" name="policy_name" class="form-control" value="<?=$myLifeInsuranceInDetail[0]['policy_name']?>">
                                                  <?php if ($errors->has('policy_name')) ?><p class="help-block"><?=$errors->first('policy_name')?></p>   
                                                </div>
                                            </div><!-- form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Policy Holder Name :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_holder_name')) echo 'has-error' ?>">
												
												<input type="text" name="policy_holder_name" class="form-control" value="<?=$myLifeInsuranceInDetail[0]['policy_holder_name']?>">
                                                  <?php if ($errors->has('policy_holder_name')) ?><p class="help-block"><?=$errors->first('policy_holder_name')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group ">
                                                <label class="col-sm-4 control-label">Premium :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('premium')) echo 'has-error' ?>">
												
												<input type="text" name="premium" class="form-control" value="<?=$myLifeInsuranceInDetail[0]['premium']?>">
                                                  <?php if ($errors->has('premium')) ?><p class="help-block"><?=$errors->first('premium')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Mode :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('mode')) echo 'has-error' ?>">
												<select type="text" onchange="updateDateOnChange();" name="mode" class="form-control"  id="mode" value="<?=$myLifeInsuranceInDetail[0]['mode']?>" width="100%">
													<option value="Yearly" >Yearly</option>
													<option value="Half Yearly">Half Yearly</option>
													<option value="Quarterly">Quarterly</option>
													<option value="Monthly">Monthly</option>
                                                   </select>
												   <?php if ($errors->has('mode')) ?><p class="help-block"><?=$errors->first('mode')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											
											
											<div class="form-group" style="<?=$belongsToDisplay?>">
                                                <label class="col-sm-4 control-label">Belongs To :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('belongs_to')) echo 'has-error' ?>">
												<input type="text" name="belongs_to" class="form-control" value="<?=$myLifeInsuranceInDetail[0]['belongs_to']?>">
                                                   <?php if ($errors->has('belongs_to')) ?><p class="help-block"><?=$errors->first('belongs_to')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											
											</div>
											<div class="col-md-6">
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Premium Payment Term :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('premium_payment_term')) echo 'has-error' ?>">
												<input type="text" name="premium_payment_term" class="form-control" value="<?=$myLifeInsuranceInDetail[0]['premium_payment_term']?>">
                                                   <?php if ($errors->has('premium_payment_term')) ?><p class="help-block"><?=$errors->first('premium_payment_term')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Policy Term :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_term')) echo 'has-error' ?>">
												<input type="text" name="policy_term" class="form-control" value="<?=$myLifeInsuranceInDetail[0]['policy_term']?>">
                                                   <?php if ($errors->has('policy_term')) ?><p class="help-block"><?=$errors->first('policy_term')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Policy Start Date :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_start_date')) echo 'has-error' ?>">
												<input type="text" name="policy_start_date" id="startDate" class="form-control" value="<?=convertDate($myLifeInsuranceInDetail[0]['policy_start_date']);?>">
                                                   <?php if ($errors->has('policy_start_date')) ?><p class="help-block"><?=$errors->first('policy_start_date')?></p>   
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Renewal Date :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_renewal_date')) echo 'has-error' ?>">
												<input type="text" name="policy_renewal_date" id="renewalDate" class="form-control" value="<?=convertDate($myLifeInsuranceInDetail[0]['renewal_date']);?>" readonly>
                                                   <?php if ($errors->has('policy_renewal_date')) ?><p class="help-block"><?=$errors->first('policy_renewal_date')?></p>   
                                                </div>
                                            </div><!-- form-group -->
																						<div class="form-group">
                                                <label class="col-sm-4 control-label">Policy maturity date :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('policy_maturity_date')) echo 'has-error' ?>">
												<input type="text" name="policy_maturity_date" id="policy_maturity_date" class="form-control" value="<?=convertDate($myLifeInsuranceInDetail[0]['policy_maturity_date']);?>">
                                                  <?php if ($errors->has('policy_maturity_date')) ?><p class="help-block"><?=$errors->first('policy_maturity_date')?></p>    
                                                </div>
                                            </div><!-- form-group -->
											<div class="form-group">
                                                <label class="col-sm-4 control-label">Sum Assured :</label>
                                                <div class="col-sm-6 <?php if ($errors->has('sum_assured')) echo 'has-error' ?>">
												<input type="text" name="sum_assured" class="form-control" value="<?=$myLifeInsuranceInDetail[0]['sum_assured']?>">
                                                 <?php if ($errors->has('sum_assured')) ?><p class="help-block"><?=$errors->first('sum_assured')?></p>     
                                                </div>
                                            </div><!-- form-group -->
										</div>	
                                        </div><!-- panel-body -->
                                      
                                        <div class="panel-footer" style="text-align:center;">
                                            <button type="submit" class="btn btn-primary mr5">Edit</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div><!-- panel-footer -->
                                    </div><!-- panel-default -->
                                </form>
                            </div>
						</div>
                    </div><!-- contentpanel -->
                    
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>
		<?php
			include('footer.php');
		?>
    </body>
</html>

	
  <script>
  function updateDate(){
		var mode = $('#mode').val();
		var startDate = $('#startDate').val();
		//alert(startDate);
		var formateStartDate = startDate.split('/');
		//alert(formateStartDate[1]);
		var formateStartDate = formateStartDate[1]+'/'+formateStartDate[0]+'/'+formateStartDate[2];
		var startDate = new Date($('#startDate').val());
		$('#startDate').val(formateStartDate);
		if(mode == 'Yearly'){
			var noOfdays = 365;
		}
		if(mode == 'Half Yearly'){
			var noOfdays = 182;
		}
		if(mode == 'Quarterly'){
			var noOfdays = 90;
		}
		if(mode == 'Monthly'){
			var noOfdays = 30;
		}
		
		
		var days = parseInt(noOfdays);
		
        if(!isNaN(startDate.getTime())){
            startDate.setDate(startDate.getDate() + days);
            
            $("#renewalDate").val(startDate.toInputFormat());
        } else {
            alert("Invalid Date");  
        }
	}
	
	function updateDateOnChange(){
		var mode = $('#mode').val();
		var startDate = $('#startDate').val();
		//alert(startDate);
		var formateStartDate = startDate.split('/');
		//alert(formateStartDate[1]);
		var formateStartDate = formateStartDate[1]+'/'+formateStartDate[0]+'/'+formateStartDate[2];
		var startDate = new Date(formateStartDate);
		//$('#startDate').val(formateStartDate);
		if(mode == 'Yearly'){
			var noOfdays = 365;
		}
		if(mode == 'Half Yearly'){
			var noOfdays = 182;
		}
		if(mode == 'Quarterly'){
			var noOfdays = 90;
		}
		if(mode == 'Monthly'){
			var noOfdays = 30;
		}
		
		
		var days = parseInt(noOfdays);
		alert(startDate.getTime());
        if(!isNaN(startDate.getTime())){
            startDate.setDate(startDate.getDate() + days);
            
            $("#renewalDate").val(startDate.toInputFormat());
        } 
	}
  
  $(function() {
  
	
    $( "#startDate" ).datepicker({onClose: function() { 
		
		updateDate();
		
	}});
	 $( "#policy_maturity_date" ).datepicker({dateFormat:'dd/mm/yy'});
	
	Date.prototype.toInputFormat = function() {
       var yyyy = this.getFullYear().toString();
       var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = this.getDate().toString();
       return  (dd[1]?dd:"0"+dd[0])+ "/" + (mm[1]?mm:"0"+mm[0]) + "/" + yyyy; // padding
		}
 
  });
  
  </script>
