		<script src="<?=$baseUrl?>theme/js/jquery-1.11.1.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/bootstrap.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/modernizr.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/pace.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/retina.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/jquery.cookies.js"></script>
		

        <script src="<?=$baseUrl?>theme/js/flot/jquery.flot.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/flot/jquery.flot.resize.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/flot/jquery.flot.spline.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/jquery.sparkline.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/morris.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/raphael-2.1.0.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/bootstrap-wizard.min.js"></script>
        <script src="<?=$baseUrl?>theme/js/select2.min.js"></script>
	

		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable( {
					"aaSorting": [[ 0, "asc" ]]
				} );
				
				$(window).scroll(function() {
			var scrollPos = $(window).scrollTop();
			var max = $(document).height() - $(window).height(); // Max avilable scroll position
			
            if ($(this).scrollTop() > 10) {
				var opacity = scrollPos / max;
                $('.headerwrapper').css({"background-color":'rgba(3, 192, 179, '+opacity+')'});
                //$('.headerwrapper').css({"opacity":opacity});
            }
			else{
				$('.headerwrapper').css({"background-color":"transparent","opacity":1});
			}
			if($(window).scrollTop() + $(window).height() == $(document).height()){
                //$('.headerwrapper').css("background", "transparent");
				 $('.headerwrapper').css({"background-color":'#03c0b3'});
                $('.headerwrapper').css({"opacity":1});
            }
			
        });
			} );
		</script>
		
	<script type="text/javascript" language="javascript" src="<?=$baseUrl?>theme/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="<?=$baseUrl?>theme/js/jquery.dataTables.js"></script>
		
		
		<!--Choosen-->
		 <script src="<?=$baseUrl?>theme/choosen/chosen.jquery.js" type="text/javascript"></script>
  <script src="<?=$baseUrl?>theme/choosen/prism.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
	
	
	//menu collapse
	var collapse = false; 
	
		$('.menu-collapse').on('click',function(){
			if(!collapse){
				$('.headerwrapper').addClass('collapsed');
				$('.mainwrapper').addClass('collapsed');
				collapse = true;
			}
			else{
				$('.headerwrapper').removeClass('collapsed');
				$('.mainwrapper').removeClass('collapsed');
				collapse = false;
			}
		});

	
  </script>
  
 
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
