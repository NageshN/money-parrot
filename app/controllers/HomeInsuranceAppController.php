<?php
class HomeInsuranceAppController extends BaseController{
	public function homeInsurance($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
				$result = InsuranceHome :: homeInsurance($data['user_id']);
				return array('status'=>'success','response'=>'fetch home insurance success',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		
		public function homeInsuranceAdd($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
			$data['policy_doc'] = '';
			$image = Input::file('policy_doc');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$data['policy_doc'] = 'theme/uploads/'.$filename;
			}
			$result = InsuranceHome::homeInsuranceAdd($data);
			if($result == 409){
				return array('status'=>'failure','response'=>'policy number already exist');
			}
			if($result == 1){
				return array('status'=>'success','response'=>'Home insurance added successfully');
			}
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		
		public function homeInsuranceView($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
				$result = InsuranceHome :: homeInsuranceView($data['id']);
				return array('status'=>'success','response'=>'fetch home insurance success',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		public function homeInsuranceEdit($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
			$data['policy_doc'] = '';
			$image = Input::file('policy_doc');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$data['policy_doc'] = 'theme/uploads/'.$filename;
			}
			$result = InsuranceHome :: homeInsuranceEditPost($data);
			return array('status'=>'success','response'=>'edited Home insurance successfully',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		public function homeInsuranceDelete($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
			$result = InsuranceHome :: homeInsuranceDelete($data['id']);
			if($result == 0){
				return array('status' => "failure","response" => "policy Number not found");
			}
			if($result == 1){
				return array('status' => "success","response" => "Policy has been Deleted");
			}
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
}
?>