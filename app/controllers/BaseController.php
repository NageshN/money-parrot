<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected $notification;

    public function __construct() 
    {
        // Fetch the Site Settings object
		$userId = Session::get('userInfo');	
		if (Auth::check())
		{
		$userInfo = Session::get('userInfo');
		$result = Notification :: getNotification($userInfo['id']);
		
		Session::put('notification',$result);
       /*  $this->notification = Notification::where('user_id','=',$id['id'])
											->where('insurance_type','=','me')
											->get();
        View::share('notification', $this->notification);
		$myInsurance = $this->notification;
		
		

		$this->notification = Notification::where('user_id','=',$id['id'])
											->where('insurance_type','!=','me')
											->get();
        View::share('notification', $this->notification);
		$myFamilyInsurance = $this->notification;
		Session::put('notification',json_encode(array_merge(json_decode($myInsurance,true),json_decode($myFamilyInsurance,true))));
		 */
		 }
    }
	
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
