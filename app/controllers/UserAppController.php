<?php
	class UserAppController extends BaseController {
		
		public function showAllUser(){
			$users = User::showAllUser();
			return $users;
		}
		public function signup(){
			return View :: make('signup');
		}
		public function signupPost(){
			$userData = array('full_name' => Input::get('full_name'),
								'email_id' => Input::get('emailid'),
								'phone_number' => Input::get('phone_number'),
								'password' => Input::get('password'),
								'registeredThrough' => 'Self'
								
								);
			$messages = array(
    'same'    => 'The :attribute and :other must match.',
    'size'    => 'The :attribute must be exactly :size.',
    'between' => 'The :attribute must be between :min - :max.',
    'in'      => 'The :attribute must be one of the following types: :values',
	'required' => 'Please fill out :attribute field'
	);
			$validator = Validator::make(Input::all(), User::$rules,$messages);
			//dd($validator->fails());
			
			if ($validator->fails()) {

				// get the error messages from the validator
				$messages = $validator->messages();
				
				// redirect our user back to the form with the errors from the validator
				
				return Redirect::to('signup')->withErrors($messages)->with('fieldValue',$userData);

			} 
			else {
				//Call create user function
				User::createUser('someType', $userData);
				return App :: make('UserAppController')->loginPost();
				
			}

			
	
		}
		
		
		public function login(){
			/* Run a authentication check to see if we are already logged in */
			
			if (Auth::check())
			{
				$userInfo = Session::get('userInfo');
				/* Is this login via a 'Remember Me' method */
				if (Auth::viaRemember())
				{
					/* If yes ,pass a variable to the 'admin' page */
					
					
					return Redirect::intended('home')->with('rememberMe', 1);
				}
				else
				{
				
					
					return Redirect::intended('home')->with('userInfo',$userInfo);
				}
			}
			else
			{
				return View::make('loginForm');
			}
		}
		
		public function loginPost(){
			 $userdata = array(
				'email_id' => Input::get('emailid'),
				'password' => Input::get('password')
			);
		 
			/* Check if 'Remember Me' is checked */
			if(Input::get('persist') == 'on')
				$isAuth = Auth::attempt($userdata, true);
			else
				$isAuth = Auth::attempt($userdata);
			
			if($isAuth)
			{
				// we are now logged in, go to home
				$users = User :: where('email_id','=',$userdata['email_id'])->first();
				//$users = Crypt::decrypt($users[0]);
				Session::put('userInfo', $users);
				//$userInfo = Session::get('userInfo');
				return Redirect::intended('home');
			}
			else
			{
				return Redirect::to('login')->with('message','Login Failed');
			}
		}
		public function home(){
			//Session::put('userInfo','nagesh');
			Session::put('pageName','home');
			$userInfo = Session::get('userInfo');
			return View :: make('home')->with('userInfo',$userInfo);
		}
		
		
		
		public function loginWithFacebook() {

    // get data from input
    $code = Input::get( 'code' );

    // get fb service
    $fb = OAuth::consumer( 'Facebook' );

    // check if code is valid

    // if code is provided get user data and sign in
    if ( !empty( $code ) ) {

        // This was a callback request from facebook, get the token
        $token = $fb->requestAccessToken( $code );

        // Send a request with it
        $resultFromFacebook = json_decode( $fb->request( '/me' ), true );
		
       /* $message = 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['name'] ;
        echo $message. "<br/>";  */ 
		
        //Var_dump
        //display whole array().
		//dd($result);
		//App :: make('UserAppController')->loginPost();
		//return $resultFromFacebook;
		$result = User::createUserFromFacebook($resultFromFacebook);
	
		if($result == 409)
			{
				return Redirect::to('login')->with('message','Email id already Exist');
				
			}
		else if($result)
			{
				// we are now logged in, go to home
				$user = User::where('social_user_id','=',$resultFromFacebook['id'])->first();
				Session::put('userInfo',$user);
				$isAuth = Auth::login($user);
				
				return Redirect :: intended('home');
				
			}
		else{
				return Redirect::to('login')->with('message','Login With Facebook Failed');
			}

    }
    // if not ask for permission first
    else {
        // get fb authorization
        $url = $fb->getAuthorizationUri();

        // return to facebook login url
         return Redirect::to( (string)$url );
    }

}
		
	public function loginWithGoogle() {

    // get data from input
    $code = Input::get( 'code' );

    // get google service
    $googleService = OAuth::consumer( 'Google' );

    // check if code is valid

    // if code is provided get user data and sign in
    if ( !empty( $code ) ) {

        // This was a callback request from google, get the token
        $token = $googleService->requestAccessToken( $code );

        // Send a request with it
        $resultFromGoogle = json_decode( $googleService->request( 'https://www.googleapis.com/oauth2/v1/userinfo' ), true );
		
		$result = User::createUserFromGoogle($resultFromGoogle);
		
		if($result == 409)
			{
				return Redirect::to('login')->with('message','Email id already Exist');
				
			}
		else if($result)
			{
				// we are now logged in, go to home
				$user = User::where('social_user_id','=',$resultFromGoogle['id'])->first();
				Session::put('userInfo',$user);
				$isAuth = Auth::login($user);
				
				return Redirect::intended('home');
				
			}
		else{
				return Redirect::to('login')->with('message','Login With Facebook Failed');
			}
			
			
        /* $message = 'Your unique Google user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
        echo $message. "<br/>"; */
		//dd($resultFromGoogle);
        //Var_dump
        //display whole array().
		

    }
    // if not ask for permission first
    else {
        // get googleService authorization
        $url = $googleService->getAuthorizationUri();

        // return to google login url
        return Redirect::to( (string)$url );
    }
}
		
		
		//-------------------------
		//register user starts here 
		//-------------------------
		 public function createUser($userInfo){
			$userInfo = base64_decode($userInfo);
			$userInfo = json_decode($userInfo,true);
			//$data = array('firstName' => $firstName,'lastName' => $lastName,'emailId' => $emailId,'password' => $password,'registeredThrough' =>$registeredThrough);
			
			//call model function and pass $data
			
			$result = User::createUser('someType', $userInfo);
			if($result['status'] == 'success'){
				$user = User :: where('email_id','=',$userInfo['email_id'])->get();
				$result = array('status' => "success","response" => "Create user success",'authKey' => base64_encode($user[0]['id'].'@'.time()),$user);
			}
			return $result;
			
			
		} 
		public function registerGCMid($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				return User::registerGCMid($data);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		public function setMpin($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				return User::setMpin($data);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		public function validateMpin($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				return User::validateMpin($data);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		public function resetMpin($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				return User::resetMpin($data);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		
		public function changeNotificationSettings($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				return User::changeNotificationSettings($data);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		
		//-----------------------
		//register user ends here 
		//-----------------------
		
		public function userLogin($data){
			$data = base64_decode($data);
			$data = json_decode($data,true);
		
			/*$result = User::userLogin($data); */	
				$isAuth = Auth::attempt($data);
			
			if($isAuth)
			{
				$userData = User :: where('email_id', '=' ,$data['email_id'])->get();
				$result = array('status' => 'success','response' => 'user login success','authKey' => base64_encode($userData[0]['id'].'@'.time()),$userData);
				return $result;
			}
			else
			{
				$result = array('status' => 'failure','response' => 'user login failure');
				return $result;
			}
			
			
		}
		
		public function loginWithFacebookByApp($userDataFromFacebook){
			$userDataFromFacebook = base64_decode($userDataFromFacebook);
			$userDataFromFacebook = json_decode($userDataFromFacebook,true);
			$result = User::createUserFromFacebook($userDataFromFacebook);
			if($result == 409)
			{
				return array('status' => 'failure','response' => 'Email Id already has been used');
				
			}
			else if($result)
			{
				// we are now logged in, go to home
				$userData = User :: where('email_id', '=' ,$userDataFromFacebook['email'])->get();
				$result = array('status' => 'success','response' => 'user login facebook success','authKey' => base64_encode($userData[0]['id'].'@'.time()),$userData);
				return $result;
				
			}
			else{
				return array('status' => 'failure','response' => 'Login with facebook failed');
			}
		}
		
		public function loginWithGoogleByApp($userDataFromGoogle){
			$userDataFromGoogle = base64_decode($userDataFromGoogle);
			$userDataFromGoogle = json_decode($userDataFromGoogle,true);
			$result = User::createUserFromGoogle($userDataFromGoogle);
		
			if($result == 409)
			{
				return array('status' => 'failure','response' => 'Email Id already has been used');
				
			}
			else if($result)
			{
				// we are now logged in, go to home
				$userData = User :: where('email_id', '=' ,$userDataFromGoogle['email'])->get();
				$result = array('status' => 'success','response' => 'user login gmail success','authKey' => base64_encode($userData[0]['id'].'@'.time()),$userData);
				return $result;
				
			}
			else{
				return array('status' => 'failure','response' => 'Login with gmail failed');
			}
		}
		
		public function forgotPassword(){
			$emailId = Input :: get('emailid');
			$user = User :: where('email_id', '=' ,$emailId)->get();
			$noOfUsers = count($user);
			if($noOfUsers > 0){	
				$to = $emailId;
				$name = 'Money Parrot';
				$fromEmail = 'hello@moneyparrot.com';
				
				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
				$headers .= 'From: '.$name.'<'.$fromEmail.'>'. "\r\n";
				$subject = "Forgot Password";
				$link = "http://codewave.co.in/testbW9uZXkjQCE=/moneyparrot/changePassword?email=".$emailId;
				$message = 'Please find the link below to change password <a href="'.$link.'" target="_blank">'.$link;
				//$headers .= 'Cc:'. $fromEmail."\r\n";
				$mail = mail($to,$subject,$message,wordwrap($headers));
				if($mail){
					//$forgotPasswordLinkTime = time() + 60 * 60 * 24 * 365; // one year
					$forgotPasswordLinkTime = time() + 60 * 60; // one hour
					Session::put('forgotPasswordLinkTime', $forgotPasswordLinkTime);
					$session = Session :: get('forgotPasswordLinkTime');
					$result = array('status'=>'success','response'=>'Email has been sent',"session" => $session);
					return Redirect::to('login')->with('message','Email has been sent');
				}
				else{
					$result = array('status'=>'failure','response'=>'email has not sent');
					return Redirect::to('login')->with('message','Email has not sent Please try again');
				}
				
			}
			else{
				$result = array('status'=>'failure','response'=>'email id does not exist');
				return Redirect::to('login')->with('message','Email id does not exist');
			}
		}
		
		public function forgotPasswordByApp($emailId){
			$emailId = base64_decode($emailId);
			$emailId = json_decode($emailId,true);
			$emailId = $emailId['EmailId'];
			$user = User :: where('email_id', '=' ,$emailId)->get();
			$noOfUsers = count($user);
			if($noOfUsers > 0){
				$to = $emailId;
				$name = 'Money Parrot';
				$fromEmail = 'hello@moneyparrot.com';
				
				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
				$headers .= 'From: '.$name.'<'.$fromEmail.'>'. "\r\n";
				$subject = "Forgot Password";
				$link = "http://codewave.co.in/moneyparrot/changePassword?email=".$emailId;
				$message = 'Please find the link below to change password <a href="'.$link.'" target="_blank">'.$link;
				//$headers .= 'Cc:'. $fromEmail."\r\n";
				$mail = mail($to,$subject,$message,wordwrap($headers));
				if($mail){
					//$forgotPasswordLinkTime = time() + 60 * 60 * 24 * 365; // one year
					$forgotPasswordLinkTime = time() + 60 * 60; // one min
					Session::put('forgotPasswordLinkTime', $forgotPasswordLinkTime);
					$session = Session :: get('forgotPasswordLinkTime');
					return array('status'=>'success','response'=>'email has been sent',"session" => $session);
				}
				else{
					return array('status'=>'failure','response'=>'email has not sent');
				}
				
			}
			else{
				return array('status'=>'failure','response'=>'email id does not exist');
			}
		}
		
		public function changePasswordView(){
			return View :: make('changePassword');
		}
		public function resetMpinView(){
			return View :: make('reset_mpin');
		}
		
		public function resetMpinViewPost(){
			$userData = array(	'email' => Input::get('email_id'),
								'm_pin' => Input::get('m_pin')
								
								);
			
			$validator = Validator::make(Input::all(), User::$rulesForResetMpin);
			
			if ($validator->fails()) {
				$messages = $validator->messages();
				
				return Redirect::to('resetMpin?email='.base64_encode($userData['email']))->withErrors($messages);

			}
			else{
				$result = User :: resetMpinWeb($userData);
				
				if($result['status'] == 'success'){
					
					return Redirect::to('resetMpin?email='.base64_encode($userData['email']))->with('message',true);
				}
			}
		}
		public function changePasswordPost(){
			$userData = array(	'email' => Input::get('email_id'),
								'password' => Input::get('password')
								
								);
			
			$validator = Validator::make(Input::all(), User::$rulesForChangePassword);
			
			if ($validator->fails()) {
				$messages = $validator->messages();
				
				return Redirect::to('changePassword?email='.$userData['email'])->withErrors($messages);

			}
			else{
				$result = User :: changePassword($userData);
				
				if($result['status'] == 'success'){
					
					return Redirect::to('login');
				}
			}
		}
		
		public function showNoti(){
			return $result = InsuranceLife :: getMyInsuranceRenewalInfo('me');
			
		}
		
	}
?>