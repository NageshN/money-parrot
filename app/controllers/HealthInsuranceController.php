<?php
	class HealthInsuranceController extends BaseController{
		public function healthInsurance(){
			Session::put('pageName','healthInsurance');
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$result = InsuranceHealth :: healthInsurance($userId);
			
			Session::put('healthInsurance',$result);
			return View :: make('healthInsurance');
		}
		
		public function healthInsuranceAdd(){
			Session::put('pageName','healthInsurance');
			return View :: make('healthInsuranceAdd');
		}
		public function healthInsuranceAddPost(){
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$imagePath = '';
			$image = Input::file('file');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('file')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$imagePath = 'theme/uploads/'.$filename;
			}
			$policyDocPath = '';
			$policyDoc = Input::file('policy_doc');
			if($policyDoc != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$policyDoc->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$policyDocPath = 'theme/uploads/'.$filename;
			}
			
			$inuranceInfo = array('company_name' => Input::get('company_name'),
									'policy_number' => Input::get('policy_number'),
									'policy_holder_name' => Input::get('policy_holder_name'),
									'user_id' => $userId,
									'product_name' => Input::get('product_name'),
									'policy_type' => Input::get('policy_type'),
									'premium' => Input::get('premium'),
									'tenure' => Input::get('tenure'),
									'policy_start_date' => Input::get('policy_start_date'),
									'policy_renewal_date' => Input::get('policy_renewal_date'),
									'policy_end_date' => Input::get('policy_end_date'),
									'sum_assured' => Input::get('sum_assured'),
									'health_card_image' => $imagePath,
									'policy_doc' => $policyDocPath
									);
			//return $inuranceInfo;
		
			$validator = Validator::make(Input::all(), InsuranceHealth::$healthRules);
			//dd($validator->fails());
			if ($validator->fails()) {
				// get the error messages from the validator
				$messages = $validator->messages();
				// redirect our user back to the form with the errors from the validator
				return Redirect::to('generalInsurance/health/add')->withErrors($messages)->with('fieldValue',$inuranceInfo);
			} 
			else{
				$result = InsuranceHealth :: healthInsuranceAdd($inuranceInfo);
				if($result == 409){
					$messages = "Policy Number already Exist";
					return Redirect::to('generalInsurance/health/add')->with('errorMessage',$messages);
				}
				else{
					return Redirect::to('generalInsurance/health')->with('message','Insurance has been created Successfully.');
				}
				
			}	
		}
		
		
		public function healthInsuranceView($id){
			Session::put('pageName','healthInsurance');
			$result = InsuranceHealth :: healthInsuranceView($id);
			Session :: put('healthInsuranceView',$result);
			return View :: make('healthInsuranceView');
		}
		
		public function healthInsuranceEdit($id){
			Session::put('pageName','healthInsurance');
			$result = InsuranceHealth :: healthInsuranceView($id);
	
			Session :: put('healthInsuranceView',$result);
			return View :: make('healthInsuranceEdit');
		}
		
		public function healthInsuranceEditPost($id){
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$imagePath = '';
			$image = Input::file('file');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('file')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$imagePath = 'theme/uploads/'.$filename;
			}
			$policyDocPath = '';
			$policyDoc = Input::file('policy_doc');
			if($policyDoc != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$policyDoc->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$policyDocPath = 'theme/uploads/'.$filename;
			}
			 $inuranceInfo = array(
									'id' => $id,
									'company_name' => Input::get('company_name'),
									'policy_number' => Input::get('policy_number'),
									'policy_holder_name' => Input::get('policy_holder_name'),
									'user_id' => $userId,
									'product_name' => Input::get('product_name'),
									'policy_type' => Input::get('policy_type'),
									'premium' => Input::get('premium'),
									'tenure' => Input::get('tenure'),
									'policy_start_date' => Input::get('policy_start_date'),
									'policy_renewal_date' => Input::get('policy_renewal_date'),
									'policy_end_date' => Input::get('policy_end_date'),
									'sum_assured' => Input::get('sum_assured'),
									'health_card_image' => $imagePath,
									'policy_doc' => $policyDocPath
									);
			
			$validator = Validator::make(Input::all(), InsuranceHealth::$healthEditRules);
			if ($validator->fails()) {
				// get the error messages from the validator
				$messages = $validator->messages();
				// redirect our user back to the form with the errors from the validator
				return Redirect::to('generalInsurance/health/edit/'.$id.'')->withErrors($messages)->with('fieldValue',$inuranceInfo);
			} 
			else{
				$result = InsuranceHealth :: healthInsuranceEditPost($inuranceInfo);
				if($result == 409){
					$messages = "Policy Number already Exist";
					return Redirect::to('generalInsurance/health/edit/'.$id.'')->with('errorMessage',$messages);
				}
				else{
					return Redirect::to('generalInsurance/health')->with('message','Insurance has been edited Successfully.');
				}
				
			}
		}
		
		public function healthInsuranceDelete($id){
			$result = InsuranceHealth :: healthInsuranceDelete($id);
			if($result == 0){
				return array('status' => "failure","response" => "policy Number not found");
			}
			if($result == 1){
				return array('status' => "success","response" => "Policy has been Deleted");
			}
		}
		
	}
	
?>