<?php
	class TravelInsuranceController extends BaseController{
		public function travelInsurance(){
			
			Session::put('pageName','travelInsurance');
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$result = InsuranceTravel :: travelInsurance($userId);
			Session::put('travelInsurance',$result);
			return View :: make('travelInsurance');
		}
		
		public function travelInsuranceAdd(){
			Session::put('pageName','travelInsurance');
			return View :: make('travelInsuranceAdd');
		}
		public function travelInsuranceAddPost(){
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$image = Input::file('policy_doc');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$imagePath = 'theme/uploads/'.$filename;
			}
			
			
			  $inuranceInfo = array('company_name' => Input::get('company_name'),
									'policy_number' => Input::get('policy_number'),
									'policy_holder_name' => Input::get('policy_holder_name'),
									'user_id' => $userId,
									'policy_name' => Input::get('policy_name'),
									'period' => Input::get('period'),
									'premium' => Input::get('premium'),
									'policy_doc' => $imagePath,
									'policy_start_date' => Input::get('policy_start_date'),
									'policy_renewal_date' => Input::get('policy_renewal_date'),
									'policy_end_date' => Input::get('policy_end_date'),
									
									
									);
		
		
			$validator = Validator::make(Input::all(), InsuranceTravel::$travelRules);
			//dd($validator->fails());
			if ($validator->fails()) {
				// get the error messages from the validator
				$messages = $validator->messages();
				// redirect our user back to the form with the errors from the validator
				return Redirect::to('generalInsurance/travel/add')->withErrors($messages)->with('fieldValue',$inuranceInfo);
			} 
			else{
				
				$result = InsuranceTravel :: travelInsuranceAdd($inuranceInfo);
				if($result == 409){
					$messages = "Policy Number already Exist";
					return Redirect::to('generalInsurance/travel/add')->with('errorMessage',$messages);
				}
				else{
					return Redirect::to('generalInsurance/travel')->with('message','Insurance has been created Successfully.');
				}
				
			}	
		}
		
		
		public function travelInsuranceView($id){
			$result = InsuranceTravel :: travelInsuranceView($id);
			Session :: put('travelInsuranceView',$result);
			return View :: make('travelInsuranceView');
		}
		
		public function travelInsuranceEdit($id){
		
			$result = InsuranceTravel :: travelInsuranceView($id);
	
			Session :: put('travelInsuranceView',$result);
			return View :: make('travelInsuranceEdit');
		}
		
		public function travelInsuranceEditPost($id){
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$imagePath = '';
			$image = Input::file('policy_doc');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$imagePath = 'theme/uploads/'.$filename;
			}
			
			$inuranceInfo = array(
									'id' => $id,
									'company_name' => Input::get('company_name'),
									'policy_number' => Input::get('policy_number'),
									'policy_holder_name' => Input::get('policy_holder_name'),
									'user_id' => $userId,
									'policy_name' => Input::get('policy_name'),
									'period' => Input::get('period'),
									'policy_doc' => $imagePath,
									'premium' => Input::get('premium'),
									
									'policy_start_date' => Input::get('policy_start_date'),
									
									'policy_end_date' => Input::get('policy_end_date'),
									
									);
			
			$validator = Validator::make(Input::all(), InsuranceTravel::$travelEditRules);
			if ($validator->fails()) {
				// get the error messages from the validator
				$messages = $validator->messages();
				// redirect our user back to the form with the errors from the validator
				return Redirect::to('generalInsurance/travel/edit/'.$id.'')->withErrors($messages)->with('fieldValue',$inuranceInfo);
			} 
			else{
				$result = InsuranceTravel :: travelInsuranceEditPost($inuranceInfo);
				if($result == 409){
					$messages = "Policy Number already Exist";
					return Redirect::to('generalInsurance/travel/edit/'.$id.'')->with('errorMessage',$messages);
				}
				else{
					return Redirect::to('generalInsurance/travel')->with('message','Insurance has been edited Successfully.');
				}
				
			}
		}
		
		public function travelInsuranceDelete($id){
			$result = InsuranceTravel :: travelInsuranceDelete($id);
			if($result == 0){
				return array('status' => "failure","response" => "policy Number not found");
			}
			if($result == 1){
				return array('status' => "success","response" => "Policy has been Deleted");
			}
		}
		
	}
	
?>