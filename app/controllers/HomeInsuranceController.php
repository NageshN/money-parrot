<?php
	class HomeInsuranceController extends BaseController{
		public function homeInsurance(){
			Session::put('pageName','homeInsurance');
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$result = InsuranceHome :: homeInsurance($userId);
			Session::put('homeInsurance',$result);
			return View :: make('homeInsurance');
		}
		
		public function homeInsuranceAdd(){
			Session::put('pageName','homeInsurance');
			return View :: make('homeInsuranceAdd');
		}
		public function homeInsuranceAddPost(){
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$imagePath = '';
			
			$image = Input::file('policy_doc');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$imagePath = 'theme/uploads/'.$filename;
			}
			
		 	$inuranceInfo = array('company_name' => Input::get('company_name'),
									'policy_number' => Input::get('policy_number'),
									'user_id' => $userId,
									'home_name' => Input::get('home_name'),
									'policy_name' => Input::get('policy_name'),
									'policy_term' => Input::get('policy_term'),
									'premium' => Input::get('premium'),
									'policy_doc' => $imagePath,
									'policy_start_date' => Input::get('policy_start_date'),
									'policy_renewal_date' => Input::get('policy_renewal_date'),
									'policy_end_date' => Input::get('policy_end_date'),
									
									
									);
			
		
			$validator = Validator::make(Input::all(), InsuranceHome::$homeRules);
			//dd($validator->fails());
			if ($validator->fails()) {
				// get the error messages from the validator
				$messages = $validator->messages();
				// redirect our user back to the form with the errors from the validator
				return Redirect::to('generalInsurance/home/add')->withErrors($messages)->with('fieldValue',$inuranceInfo);
			} 
			else{
				$result = InsuranceHome :: homeInsuranceAdd($inuranceInfo);
				if($result == 409){
					$messages = "Policy Number already Exist";
					return Redirect::to('generalInsurance/home/add')->with('errorMessage',$messages);
				}
				else{
					return Redirect::to('generalInsurance/home')->with('message','Insurance has been created Successfully.');
				}
				
			}	
		}
		
		
		public function homeInsuranceView($id){
			$result = InsuranceHome :: homeInsuranceView($id);
			Session :: put('homeInsuranceView',$result);
			return View :: make('homeInsuranceView');
		}
		
		public function homeInsuranceEdit($id){
		
			$result = InsuranceHome :: homeInsuranceView($id);
	
			Session :: put('homeInsuranceView',$result);
			return View :: make('homeInsuranceEdit');
		}
		
		public function homeInsuranceEditPost($id){
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$imagePath = '';
			
			$image = Input::file('policy_doc');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$imagePath = 'theme/uploads/'.$filename;
			}
			
			$inuranceInfo = array(
									'id' => $id,
									'company_name' => Input::get('company_name'),
									'policy_number' => Input::get('policy_number'),
									'user_id' => $userId,
									'policy_name' => Input::get('policy_name'),
									'policy_term' => Input::get('policy_term'),
									'home_name' => Input::get('home_name'),
									'premium' => Input::get('premium'),
									'policy_doc' => $imagePath,
									'policy_start_date' => Input::get('policy_start_date'),
									'policy_renewal_date' => Input::get('policy_renewal_date'),
									'policy_end_date' => Input::get('policy_end_date'),
									
									);
			
			$validator = Validator::make(Input::all(), InsuranceHome::$homeEditRules);
			if ($validator->fails()) {
				// get the error messages from the validator
				$messages = $validator->messages();
				// redirect our user back to the form with the errors from the validator
				return Redirect::to('generalInsurance/home/edit/'.$id.'')->withErrors($messages)->with('fieldValue',$inuranceInfo);
			} 
			else{
				$result = InsuranceHome :: homeInsuranceEditPost($inuranceInfo);
				if($result == 409){
					$messages = "Policy Number already Exist";
					return Redirect::to('generalInsurance/home/edit/'.$id.'')->with('errorMessage',$messages);
				}
				else{
					return Redirect::to('generalInsurance/home')->with('message','Insurance has been edited Successfully.');
				}
				
			}
		}
		
		public function homeInsuranceDelete($id){
			$result = InsuranceHome :: homeInsuranceDelete($id);
			if($result == 0){
				return array('status' => "failure","response" => "policy Number not found");
			}
			if($result == 1){
				return array('status' => "success","response" => "Policy has been Deleted");
			}
		}
		
	}
	
?>