<?php
	class LifeInsuranceController extends BaseController{
		public function create(){
			$result = InsuranceLife::create();
			return $result;
		}
		public function update(){
		}
		public function delete(){
		}
		public function view(){
		}
		
		public function myInsurance(){
			
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$result = InsuranceLife :: myLifeInsurance($userId);
			Session::put('pageName','myInsurance');
			Session::put('myLifeInsurance',$result);
			return View :: make('myInsurance');
		}
		
		public function familyInsurance(){
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$result = InsuranceLife :: familyLifeInsurance($userId);
			Session::put('pageName','familyInsurance');
			Session::put('familyLifeInsurance',$result);
			return View :: make('familyInsurance');
		}
		
		public function lifeInsuranceInDetail($policyNumber){
			$result = InsuranceLife :: lifeInsuranceInDetail($policyNumber);
			if($result[0]['belongs_to'] == 'me'){
				Session::put('pageName','myInsurance');
			}
			else{
				Session::put('pageName','familyInsurance');
			}
			
			Session::put('lifeInsuranceInDetail',$result);
			return View :: make('lifeInsuranceView');
		}
		public function lifeInsuranceEdit($policyNumber){
			$result = InsuranceLife :: lifeInsuranceInDetail($policyNumber);
			if($result[0]['belongs_to'] == 'me'){
				Session::put('pageName','myInsurance');
			}
			else{
				Session::put('pageName','familyInsurance');
			}
			Session::put('myLifeInsuranceInDetail',$result);
			return View :: make('lifeInsuranceEdit');
		}
		public function lifeInsuranceCreate($type){
			if($type == 'me'){
				Session::put('pageName','myInsurance');
				Session::put('type','me');
			}
			else{
				Session::put('pageName','familyInsurance');
				Session::put('type','family');
			}
			return View :: make('lifeInsuranceCreate');
		}
		public function lifeInsuranceCreatePost($type){
			
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$inuranceInfo = array('company_name' => Input::get('company_name'),
									'policy_number' => Input::get('policy_number'),
									'policy_holder_name' => Input::get('policy_holder_name'),
									'user_id' => $userId,
									'belongs_to' => Input::get('belongs_to'),
									'policy_name' => Input::get('policy_name'),
									'premium' => Input::get('premium'),
									'mode' => Input::get('mode'),
									'premium_payment_term' => Input::get('premium_payment_term'),
									'policy_term' => Input::get('policy_term'),
									
									'policy_start_date' => Input::get('policy_start_date'),
									'renewal_date' => Input::get('policy_renewal_date'),
									'policy_maturity_date' => Input::get('policy_maturity_date'),
									'sum_assured' => Input::get('sum_assured')
									);
							
								
									
									//return $date;
			
			$validator = Validator::make(Input::all(), InsuranceLife::$lifeInsuranceRules);
			
			if ($validator->fails()) {
				// get the error messages from the validator
				$messages = $validator->messages();
				// redirect our user back to the form with the errors from the validator
				return Redirect::to('lifeInsuranceCreate/'.$type)->withErrors($messages)->with('fieldValue',$inuranceInfo);
			} 
			else{
				$result = InsuranceLife :: createLifeInsurance($inuranceInfo);
				if($result == 409){
					$messages = "Policy Number already Exist";
					return Redirect::to('lifeInsuranceCreate/'.$type)->with('errorMessage',$messages);
				}
				if($inuranceInfo['belongs_to'] == 'me'){
					return Redirect::to('myInsurance')->with('message','Insurance has been created Successfully.');
				}
				else{
					return Redirect::to('familyInsurance')->with('message','Insurance has been created Successfully.');
				}
				
			}			
		}
		
		
		public function lifeInsuranceEditPost($id){
			//Session::put('pageName','myInsurance');
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$inuranceInfo = array(
									'id' => $id,
									'company_name' => Input::get('company_name'),
									'policy_number' => Input::get('policy_number'),
									'policy_holder_name' => Input::get('policy_holder_name'),
									'user_id' => $userId,
									'belongs_to' => Input::get('belongs_to'),
									'policy_name' => Input::get('policy_name'),
									'premium' => Input::get('premium'),
									'mode' => Input::get('mode'),
									'premium_payment_term' => Input::get('premium_payment_term'),
									'policy_term' => Input::get('policy_term'),
									
									'policy_start_date' => Input::get('policy_start_date'),
									'renewal_date' => Input::get('policy_renewal_date'),
									'policy_maturity_date' => Input::get('policy_maturity_date'),
									'sum_assured' => Input::get('sum_assured')
									);
									//echo $inuranceInfo['policy_start_date'];
								
									
									//return $date;
			
			$validator = Validator::make(Input::all(), InsuranceLife::$lifeInsuranceEditRules);
			//dd($validator->fails());
			if ($validator->fails()) {
				// get the error messages from the validator
				$messages = $validator->messages();
				// redirect our user back to the form with the errors from the validator
				return Redirect::to('lifeInsuranceEdit/'.Input::get('policy_number').'')->withErrors($messages)->with('fieldValue',$inuranceInfo);
			} 
			
			else{
				$result = InsuranceLife :: editLifeInsurance($inuranceInfo);
				if($result == 409){
					$messages = "Policy Number already Exist";
					return Redirect::to('lifeInsuranceCreate')->with('errorMessage',$messages);
				}
				if($inuranceInfo['belongs_to'] == 'me'){
					return Redirect::to('myInsurance')->with('message','Insurance has been edited Successfully.');;
				}
				else{
					return Redirect::to('familyInsurance')->with('message','Insurance has been edited Successfully.');;
				}
			}			
		}
		
		public static function lifeInsuranceDelete($policyNumber){
			$result = InsuranceLife :: lifeInsuranceDelete($policyNumber);
			if($result == 0){
				return array('status' => "failure","response" => "policy Number not found");
			}
			if($result == 1){
				return array('status' => "success","response" => "Policy has been Deleted");
			}
		}
	}
?>