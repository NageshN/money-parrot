<?php
class HealthInsuranceAppController extends BaseController{
	public function healthInsurance($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				$result = InsuranceHealth :: healthInsurance($userId);
				return array('status'=>'success','response'=>'fetch health insurance success',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		
		public function healthInsuranceAdd($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
			//call a model to create Life insurance
			$data['health_card_image'] = '';
			$image = Input::file('file');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('file')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$data['health_card_image'] = 'theme/uploads/'.$filename;
			}
			$data['policy_doc'] = '';
			$policyDoc = Input::file('policy_doc');
			if($policyDoc != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$policyDoc->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$data['policy_doc'] = 'theme/uploads/'.$filename;
			}
			$result = InsuranceHealth::healthInsuranceAdd($data);
			if($result == 409){
				return array('status'=>'failure','response'=>'policy number already exist');
			}
			if($result == 1){
				return array('status'=>'success','response'=>'insurance added successfully');
			}
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
			
		}
		
		public function healthInsuranceView($policyNumber){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				$result = InsuranceHealth :: healthInsuranceView($policyNumber);
				return array('status'=>'success','response'=>'fetch health insurance success',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
			
		}
		public function healthInsuranceEdit($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
			$data['health_card_image'] = '';
			$image = Input::file('file');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('file')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$data['health_card_image'] = 'theme/uploads/'.$filename;
			}
			$data['policy_doc'] = '';
			$policyDoc = Input::file('policy_doc');
			if($policyDoc != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$policyDoc->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$data['policy_doc'] = 'theme/uploads/'.$filename;
			}
			$result = InsuranceHealth :: healthInsuranceEditPost($data);
			return array('status'=>'success','response'=>'edited health insurance successfully',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		public function healthInsuranceDelete($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
			$result = InsuranceHealth :: healthInsuranceDelete($data['id']);
			if($result == 0){
				return array('status' => "failure","response" => "policy Number not found");
			}
			if($result == 1){
				return array('status' => "success","response" => "Policy has been Deleted");
			}
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
}
?>