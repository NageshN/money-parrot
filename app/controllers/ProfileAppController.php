<?php
	class ProfileAppController extends BaseController{
		public function viewProfile($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				$result = User :: viewProfile($userId);
				$response = array('status' => 'success','response' => 'fetch user info','user_detail' => $result);
			}
			else{
				$response =  array('status'=>'failure','response'=>'validation of user fails');
			}
			return $response;
		}
		
		public function editProfilePost($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
			$details = array('phone_number' => $data['phone_number'],
							'full_name' => $data['full_name'],
							'user_id' => $userId);
			$image = Input::file('file');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('file')->move($destinationPath, $filename);
				
				$imagePath = 'theme/uploads/'.$filename;
			}
			else{
				$user = User :: viewProfile($userId);
				$imagePath = $user[0]['profile_picture'];
			}
			$details['imagePath'] = $imagePath;
			$result = User::editProfile($details);
			if($result){
				
				 $user = User::where('id','=',$userId)->get();
				 return array('status' => 'success', 'response' => 'update profile','user_info' => $user);
			}
			
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
	}
?>