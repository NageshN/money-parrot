<?php
class MotorInsuranceAppController extends BaseController{
	public function motorInsurance($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				$result = InsuranceMotor :: motorInsurance($data['user_id']);
				return array('status'=>'success','response'=>'fetch motor insurance success',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		
		public function motorInsuranceAdd($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
				$data['policy_doc'] = '';
			$image = Input::file('policy_doc');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$data['policy_doc'] = 'theme/uploads/'.$filename;
			}
			//call a model to create Life insurance
			$result = InsuranceMotor::motorInsuranceAdd($data);
			if($result == 409){
				return array('status'=>'failure','response'=>'policy number already exist');
			}
			if($result == 1){
				return array('status'=>'success','response'=>'insurance added successfully');
			}
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		
		public function motorInsuranceView($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
			$result = InsuranceMotor :: motorInsuranceView($data['id']);
			return array('status'=>'success','response'=>'fetch motor insurance success',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		public function motorInsuranceEdit($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
				$data['policy_doc'] = '';
			$image = Input::file('policy_doc');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$data['policy_doc'] = 'theme/uploads/'.$filename;
			}
				$result = InsuranceMotor :: motorInsuranceEditPost($data);
				return array('status'=>'success','response'=>'edited motor insurance successfully',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
			
		}
		public function motorInsuranceDelete($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
			$result = InsuranceMotor :: motorInsuranceDelete($data['id']);
			if($result == 0){
				return array('status' => "failure","response" => "policy Number not found");
			}
			
			if($result == 1){
				return array('status' => "success","response" => "Policy has been Deleted");
			}
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
			
		}
}
?>