<?php
	class ProfileController extends BaseController{
		public function viewProfile(){
			Session::put('pageName','profile');
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$result = User :: viewProfile($userId);
			
			Session::put('usersDetails',$result);
			return View :: make('profile');
		}
		public function editProfile(){
			Session::put('pageName','profile');
			
			return View :: make('editProfile');
		}
		public function editProfilePost(){
			Session::put('pageName','profile');
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$details = array('phone_number' => Input::get('phone_number'),
							'full_name' => Input::get('full_name'),
							'user_id' => $userId);
			$image = Input::file('file');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('file')->move($destinationPath, $filename);
				
				$imagePath = 'theme/uploads/'.$filename;
			}
			else{
				$imagePath = Input::get('profile_image');
			}
			$details['imagePath'] = $imagePath;
			$result = User::editProfile($details);
			
			if($result){
				$user = User::where('id','=',$userId)->first();
				Session::put('userInfo',$user);
				return Redirect :: to('profile');
			}
		}
	}
?>