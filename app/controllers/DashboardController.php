<?php
	class DashboardController extends BaseController{
		public function home(){
			//Session::put('userInfo','nagesh');
			Session::put('pageName','home');
			$userInfo = Session::get('userInfo');
			$userId = $userInfo['id'];
			$myLifeDetails = InsuranceLife :: myLifeInsurance($userId);
				$myLifeSum = 0;
				for($i=0;$i<count($myLifeDetails);$i++){
					$myLifeSum = $myLifeSum + $myLifeDetails[$i]['sum_assured'];
					
				}
					
			$familyDetails = InsuranceLife :: familyLifeInsurance($userId);
			
				$familyLifeSum = 0;
				for($i=0;$i<count($familyDetails);$i++){
					$familyLifeSum = $familyLifeSum + $familyDetails[$i]['sum_assured'];
				}
			$motorDetails = InsuranceMotor :: motorInsurance($userId);
			$healthDetails = InsuranceHealth :: healthInsurance($userId);
			$travelDetails = InsuranceTravel :: travelInsurance($userId);
			//$homeDetails = InsuranceHome :: homeInsurance($userId);
		
			Session::put('myLifeDetails',$myLifeDetails);
			Session::put('myLifeSum',$myLifeSum);
			Session::put('familyDetails',$familyDetails);
			Session::put('familyLifeSum',$familyLifeSum);
			Session::put('motorDetails',$motorDetails);
			Session::put('healthDetails',$healthDetails);
			Session::put('travelDetails',$travelDetails);
			return View :: make('home')->with('userInfo',$userInfo);
		}
	}
?>