<?php
	class LifeInsuranceAppController extends BaseController{
		public function create($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($data['belongs_to'] == 'me'){
				$type = 'My';
			}
			else{
				$type = 'Family';
			}
			if($validateUser == 1){
				$result = InsuranceLife::createLifeInsurance($data);
				if($result == 409){
					return array('status'=>'failure','response'=>'policy number already exist');
				}
				if($result == 1){
					return array('status'=>'success','response'=>''.$type.' insurance added successfully');
				}
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
			//call a model to create Life insurance
			
		}
		
		public function update(){
		}
		public function delete(){
		}
		public function myLifeInsurance($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				$result = InsuranceLife :: myLifeInsurance($data['user_id']);
				return array('status' => 'success','response' => 'fetch all myInsurance',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		
		public function myFamilyLifeInsurance($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				$result = InsuranceLife :: familyLifeInsurance($data['user_id']);
				return array('status' => 'success','response' => 'fetch all myFamilyInsurance',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
			
		}
		
		public function lifeInsuranceInDetail($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				$result = InsuranceLife :: lifeInsuranceInDetail($data['id']);
				return array('status' => 'success','response' => 'fetch Insurance',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		
		public function editLifeInsurance($data){
			
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($data['belongs_to'] == 'me'){
				$type = 'My';
			}
			else{
				$type = 'Family';
			}
			if($validateUser == 1){
				$result = InsuranceLife :: editLifeInsurance($data);
				return array('status' => 'success','response' => 'edit '.$type.' Insurance',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		public static function lifeInsuranceDelete($data){
			
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){
				$result = InsuranceLife :: lifeInsuranceDelete($data['id']);
				if($result == 0){
					return array('status' => "failure","response" => "policy Number not found");
				}
				if($result == 1){
					return array('status' => "success","response" => "Policy has been Deleted");
				}
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
	}
?>