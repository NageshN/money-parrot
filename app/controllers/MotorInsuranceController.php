<?php
	class MotorInsuranceController extends BaseController{
		public function motorInsurance(){
			Session::put('pageName','motorInsurance');
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$result = InsuranceMotor :: motorInsurance($userId);
			Session::put('motorInsurance',$result);
			return View :: make('motorInsurance');
		}
		
		public function motorInsuranceAdd(){
			Session::put('pageName','motorInsurance');
			return View::make('motorInsuranceAdd');
		}
		public function motorInsuranceAddPost(){
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$imagePath = '';
			
			 $image = Input::file('policy_doc');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$imagePath = 'theme/uploads/'.$filename;
			}
			 $inuranceInfo = array('vehicle_type' => Input::get('vehicle_type'),
									'make' => Input::get('make'),
									'user_id' => $userId,
									'model' => Input::get('model'),
									'fuel_type' => Input::get('fuel_type'),
									'year_of_manufacture' => Input::get('year_of_manufacture'),
									'policy_doc' => $imagePath,
									'insured_declared_value' => Input::get('insured_declared_value'),
									'insurance_company_name' => Input::get('insurance_company_name'),
									'policy_number' => Input::get('policy_number'),
									'policy_start_date' => Input::get('policy_start_date'),
									'premium' => Input::get('premium'),
									'policy_renewal_date' => Input::get('policy_renewal_date'),
									);
			$validator = Validator::make(Input::all(), InsuranceMotor::$motorRules);
			//dd($validator->fails());
			if ($validator->fails()) {
				// get the error messages from the validator
				$messages = $validator->messages();
				// redirect our user back to the form with the errors from the validator
				return Redirect::to('generalInsurance/motor/add')->withErrors($messages)->with('fieldValue',$inuranceInfo);
			} 
			else{
				$result = InsuranceMotor :: motorInsuranceAdd($inuranceInfo);
				if($result == 409){
					$messages = "Policy Number already Exist";
					return Redirect::to('generalInsurance/motor/add')->with('errorMessage',$messages);
				}
				else{
					return Redirect::to('generalInsurance/motor')->with('message','Insurance has been created Successfully.');
				}
				
			}			
		}
		
		public function motorInsuranceView($policyNumber){
		Session::put('pageName','motorInsurance');
			$result = InsuranceMotor :: motorInsuranceView($policyNumber);
			
			Session :: put('motorInsuranceView',$result);
			return View :: make('motorInsuranceView');
		}
		
		public function motorInsuranceEdit($policyNumber){
			Session::put('pageName','motorInsurance');
			$result = InsuranceMotor :: motorInsuranceView($policyNumber);
			Session :: put('motorInsuranceView',$result);
			return View :: make('motorInsuranceEdit');
		}
		
		public function motorInsuranceEditPost($id){
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$imagePath = '';
			
			$image = Input::file('policy_doc');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$imagePath = 'theme/uploads/'.$filename;
			}
			$inuranceInfo = array(
									'id' => $id,
									'vehicle_type' => Input::get('vehicle_type'),
									'make' => Input::get('make'),
									'model' => Input::get('model'),
									'fuel_type' => Input::get('fuel_type'),
									'insured_declared_value' => Input::get('insured_declared_value'),
									'user_id' => $userId,
									'policy_doc' => $imagePath,
									'year_of_manufacture' => Input::get('year_of_manufacture'),
									'insurance_company_name' => Input::get('insurance_company_name'),
									'policy_number' => Input::get('policy_number'),
									'policy_start_date' => Input::get('policy_start_date'),
									'premium' => Input::get('premium'),
									'policy_renewal_date' => Input::get('policy_renewal_date'),
									);
			$validator = Validator::make(Input::all(), InsuranceMotor::$motorEditRules);
			if ($validator->fails()) {
				// get the error messages from the validator
				$messages = $validator->messages();
				// redirect our user back to the form with the errors from the validator
				return Redirect::to('generalInsurance/motor/edit/'.$policyNumber.'')->withErrors($messages)->with('fieldValue',$inuranceInfo);
			} 
			else{
				$result = InsuranceMotor :: motorInsuranceEditPost($inuranceInfo);
				if($result == 409){
					$messages = "Policy Number already Exist";
					return Redirect::to('generalInsurance/motor/edit/'.$policyNumber.'')->with('errorMessage',$messages);
				}
				else{
					return Redirect::to('generalInsurance/motor')->with('message','Insurance has been edited Successfully.');
				}
				
			}
		}
		
		public function motorInsuranceDelete($policyNumber){
			$result = InsuranceMotor :: motorInsuranceDelete($policyNumber);
			if($result == 0){
				return array('status' => "failure","response" => "policy Number not found");
			}
			if($result == 1){
				return array('status' => "success","response" => "Policy has been Deleted");
			}
		}
	}
?>