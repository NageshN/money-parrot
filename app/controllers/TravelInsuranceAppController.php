<?php
class TravelInsuranceAppController extends BaseController{
	public function travelInsurance($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
				$result = InsuranceTravel :: travelInsurance($data['user_id']);
				return array('status'=>'success','response'=>'fetch travel insurance success',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		
		public function travelInsuranceAdd($data){

			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
				$data['policy_doc'] = '';
			 $image = Input::file('policy_doc');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$data['policy_doc'] = 'theme/uploads/'.$filename;
			}

			$result = InsuranceTravel::travelInsuranceAdd($data);
			if($result == 409){
				return array('status'=>'failure','response'=>'policy number already exist');
			}
			if($result == 1){
				return array('status'=>'success','response'=>'travel insurance added successfully');
			}
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		
		public function travelInsuranceView($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
				$result = InsuranceTravel :: travelInsuranceView($data['id']);
				return array('status'=>'success','response'=>'fetch travel insurance success',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		public function travelInsuranceEdit($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
			$data['policy_doc'] = '';
			$image = Input::file('policy_doc');
			if($image != ''){
				$destinationPath = 'theme/uploads';
				$filename = time().$image->getClientOriginalName();
				$move = Input::file('policy_doc')->move($destinationPath, $filename);
				//Image::make($image->getRealPath())->resize(20, 20)->save('theme/uploads'. $filename);
				$data['policy_doc'] = 'theme/uploads/'.$filename;
			}
			$result = InsuranceTravel :: travelInsuranceEditPost($data);
			return array('status'=>'success','response'=>'edited travel insurance successfully',$result);
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
		public function travelInsuranceDelete($data){
			$data = base64_decode($data);
			$data = json_decode($data,true); 
			$userIdTime = explode("@",base64_decode($data['authKey']));
			$userId = $userIdTime[0];
			$validateUser = User :: validateUser($userId);
			if($validateUser == 1){ 
			$result = InsuranceTravel :: travelInsuranceDelete($data['id']);
			if($result == 0){
				return array('status' => "failure","response" => "policy Number not found");
			}
			if($result == 1){
				return array('status' => "success","response" => "Policy has been Deleted");
			}
			}
			else{
				return array('status'=>'failure','response'=>'validation of user fails');
			}
		}
}
?>