<?php
	class GeneralInsuranceController extends BaseController{
		public function generalInsurance(){
			Session::put('pageName','generalInsurance');
			$user = Session::get('userInfo');
			$userId = $user['id'];
			$motorDetails = InsuranceMotor :: motorInsurance($userId);
			$healthDetails = InsuranceHealth :: healthInsurance($userId);
			$homeDetails = InsuranceHome :: homeInsurance($userId);
			$travelDetails = InsuranceTravel :: travelInsurance($userId);
		
			Session::put('motorDetails',$motorDetails);
			Session::put('healthDetails',$healthDetails);
			Session::put('homeDetails',$homeDetails);
			Session::put('travelDetails',$travelDetails);
			return View :: make('generalInsurance');
		}
	}
?>