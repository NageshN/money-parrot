<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	
     /* Run a authentication check to see if we are already logged in */
    if (Auth::check())
    {
        /* Is this login via a 'Remember Me' method */
        if (Auth::viaRemember())
        {
            /* If yes ,pass a variable to the 'admin' page */
            return Redirect::to('home')->with('rememberMe', 1);
        }
        else
        {
            return Redirect::to('home');
        }
    }
    else
    {
        return View::make('loginForm');
    }
});
 





 
/* Logout the user, and redirect to the 'Login' screen */
Route::get('logout', function()
{
    Auth::logout();
    return Redirect::to('login');
	//return View::make('loginForm');
})->after('invalidate-browser-cache');

 
Route::resource('question','questionController');
Route::get('signup',array('as'=>'signup','uses'=>'UserAppController@signup'));
Route::post('signup',array('as'=>'signingup','uses'=>'UserAppController@signupPost'));
Route::get('login',array('as'=>'login','uses'=>'UserAppController@login'));
Route::get('loginWithFacebook',array('as'=>'loginWithFacebook','uses'=>'UserAppController@loginWithFacebook'));
Route::get('loginWithGoogle',array('as'=>'loginWithFacebook','uses'=>'UserAppController@loginWithGoogle'));
Route::post('login',array('as'=>'loginingin','after' => 'no-cache', 'uses'=>'UserAppController@loginPost'));

Route::get('about',array('before'=>'auth','uses'=>'UserAppController@about'));
Route::post('forgotPassword',array('uses'=>'UserAppController@forgotPassword'));
Route::post('forgotPasswordByApp/{email_id}',array('uses'=>'UserAppController@forgotPasswordByApp'));
Route::get('changePassword',array('uses'=>'UserAppController@changePasswordView'));
Route::post('changePassword',array('uses'=>'UserAppController@changePasswordPost'));

//Dashboard WEB 

Route::get('home',array('before'=>'auth','after' => 'no-cache','uses'=>'DashboardController@home'));

//Life Insurance WEB
Route::get('myInsurance',array('before'=>'auth','uses'=>'LifeInsuranceController@myInsurance'));
Route::get('familyInsurance',array('before'=>'auth','uses'=>'LifeInsuranceController@familyInsurance'));
Route::get('lifeInsuranceView/{id}',array('before'=>'auth','uses'=>'LifeInsuranceController@lifeInsuranceInDetail'));
Route::get('lifeInsuranceEdit/{id}',array('before'=>'auth','uses'=>'LifeInsuranceController@lifeInsuranceEdit'));
Route::post('lifeInsuranceEdit/{id}',array('before'=>'auth','uses'=>'LifeInsuranceController@lifeInsuranceEditPost'));
Route::get('lifeInsuranceCreate/{type}',array('before'=>'auth','uses'=>'LifeInsuranceController@lifeInsuranceCreate'));
Route::post('lifeInsuranceCreate/{type}',array('before'=>'auth','uses'=>'LifeInsuranceController@lifeInsuranceCreatePost'));
Route::post('lifeInsuranceDelete/{id}',array('before'=>'auth','uses'=>'LifeInsuranceController@lifeInsuranceDelete'));


//Life Insurance App
Route::post('createLifeInsurance/{details}',array('uses'=>'LifeInsuranceAppController@create'));
Route::get('myLifeInsurance/{details}',array('uses'=>'LifeInsuranceAppController@myLifeInsurance'));
Route::get('myFamilyLifeInsurance/{details}',array('uses'=>'LifeInsuranceAppController@myFamilyLifeInsurance'));
Route::get('lifeInsurance/{id}',array('uses'=>'LifeInsuranceAppController@lifeInsuranceInDetail'));
Route::post('editLifeInsurance/{details}',array('uses'=>'LifeInsuranceAppController@editLifeInsurance'));
Route::post('deleteLifeInsurance/{id}',array('uses'=>'LifeInsuranceAppController@lifeInsuranceDelete'));



//general Insurance
Route::get('generalInsurance',array('before'=>'auth','uses'=>'GeneralInsuranceController@generalInsurance'));


//Motor Insurance WEB 
Route::get('generalInsurance/motor',array('before'=>'auth','uses'=>'MotorInsuranceController@motorInsurance'));
Route::get('generalInsurance/motor/add',array('before'=>'auth','uses'=>'MotorInsuranceController@motorInsuranceAdd'));
Route::post('generalInsurance/motor/add',array('before'=>'auth','uses'=>'MotorInsuranceController@motorInsuranceAddPost'));
Route::get('generalInsurance/motor/view/{policyNumber}',array('before'=>'auth','uses'=>'MotorInsuranceController@motorInsuranceView'));
Route::get('generalInsurance/motor/edit/{policyNumber}',array('before'=>'auth','uses'=>'MotorInsuranceController@motorInsuranceEdit'));
Route::post('generalInsurance/motor/edit/{policyNumber}',array('before'=>'auth','uses'=>'MotorInsuranceController@motorInsuranceEditPost'));
Route::post('generalInsurance/motor/delete/{policyNumber}',array('before'=>'auth','uses'=>'MotorInsuranceController@motorInsuranceDelete'));


//Motor Insurance APP
Route::get('app/generalInsurance/motor/{userId}',array('uses'=>'MotorInsuranceAppController@motorInsurance'));
Route::post('app/generalInsurance/motor/add/{details}',array('uses'=>'MotorInsuranceAppController@motorInsuranceAdd'));
Route::get('app/generalInsurance/motor/view/{policyNumber}',array('uses'=>'MotorInsuranceAppController@motorInsuranceView'));
Route::post('app/generalInsurance/motor/edit/{details}',array('uses'=>'MotorInsuranceAppController@motorInsuranceEdit'));
Route::post('app/generalInsurance/motor/delete/{details}',array('uses'=>'MotorInsuranceAppController@motorInsuranceDelete'));

//Health Insurance WEB 
Route::get('generalInsurance/health',array('before'=>'auth','uses'=>'HealthInsuranceController@healthInsurance'));
Route::get('generalInsurance/health/add',array('before'=>'auth','uses'=>'HealthInsuranceController@healthInsuranceAdd'));
Route::post('generalInsurance/health/add',array('before'=>'auth','uses'=>'HealthInsuranceController@healthInsuranceAddPost'));
Route::get('generalInsurance/health/view/{id}',array('before'=>'auth','uses'=>'HealthInsuranceController@healthInsuranceView'));
Route::get('generalInsurance/health/edit/{id}',array('before'=>'auth','uses'=>'HealthInsuranceController@healthInsuranceEdit'));
Route::post('generalInsurance/health/edit/{id}',array('before'=>'auth','uses'=>'HealthInsuranceController@healthInsuranceEditPost'));
Route::post('generalInsurance/health/delete/{id}',array('before'=>'auth','uses'=>'HealthInsuranceController@healthInsuranceDelete'));


//Health Insurance APP
Route::get('app/generalInsurance/health/{details}',array('uses'=>'HealthInsuranceAppController@healthInsurance'));
Route::post('app/generalInsurance/health/add/{details}',array('uses'=>'HealthInsuranceAppController@healthInsuranceAdd'));
Route::get('app/generalInsurance/health/view/{id}',array('uses'=>'HealthInsuranceAppController@healthInsuranceView'));
Route::post('app/generalInsurance/health/edit/{details}',array('uses'=>'HealthInsuranceAppController@healthInsuranceEdit'));
Route::post('app/generalInsurance/health/delete/{id}',array('uses'=>'HealthInsuranceAppController@healthInsuranceDelete'));


//Home Insurance
Route::get('generalInsurance/home',array('before'=>'auth','uses'=>'HomeInsuranceController@homeInsurance'));
Route::get('generalInsurance/home/add',array('before'=>'auth','uses'=>'HomeInsuranceController@homeInsuranceAdd'));
Route::post('generalInsurance/home/add',array('before'=>'auth','uses'=>'HomeInsuranceController@homeInsuranceAddPost'));
Route::get('generalInsurance/home/view/{id}',array('before'=>'auth','uses'=>'HomeInsuranceController@homeInsuranceView'));
Route::get('generalInsurance/home/edit/{id}',array('before'=>'auth','uses'=>'HomeInsuranceController@homeInsuranceEdit'));
Route::post('generalInsurance/home/edit/{id}',array('before'=>'auth','uses'=>'HomeInsuranceController@homeInsuranceEditPost'));
Route::post('generalInsurance/home/delete/{id}',array('before'=>'auth','uses'=>'HomeInsuranceController@homeInsuranceDelete'));

//Travel Insurance
Route::get('generalInsurance/travel',array('before'=>'auth','uses'=>'TravelInsuranceController@travelInsurance'));
Route::get('generalInsurance/travel/add',array('before'=>'auth','uses'=>'TravelInsuranceController@travelInsuranceAdd'));
Route::post('generalInsurance/travel/add',array('before'=>'auth','uses'=>'TravelInsuranceController@travelInsuranceAddPost'));
Route::get('generalInsurance/travel/view/{id}',array('before'=>'auth','uses'=>'TravelInsuranceController@travelInsuranceView'));
Route::get('generalInsurance/travel/edit/{id}',array('before'=>'auth','uses'=>'TravelInsuranceController@travelInsuranceEdit'));
Route::post('generalInsurance/travel/edit/{id}',array('before'=>'auth','uses'=>'TravelInsuranceController@travelInsuranceEditPost'));
Route::post('generalInsurance/travel/delete/{id}',array('before'=>'auth','uses'=>'TravelInsuranceController@travelInsuranceDelete'));

//Home Insurance APP
Route::get('app/generalInsurance/home/{userId}',array('uses'=>'HomeInsuranceAppController@homeInsurance'));
Route::post('app/generalInsurance/home/add/{details}',array('uses'=>'HomeInsuranceAppController@homeInsuranceAdd'));
Route::get('app/generalInsurance/home/view/{id}',array('uses'=>'HomeInsuranceAppController@homeInsuranceView'));
Route::post('app/generalInsurance/home/edit/{details}',array('uses'=>'HomeInsuranceAppController@homeInsuranceEdit'));
Route::post('app/generalInsurance/home/delete/{id}',array('uses'=>'HomeInsuranceAppController@homeInsuranceDelete'));

//Home Insurance APP
Route::get('app/generalInsurance/travel/{userId}',array('uses'=>'TravelInsuranceAppController@travelInsurance'));
Route::post('app/generalInsurance/travel/add/{details}',array('uses'=>'TravelInsuranceAppController@travelInsuranceAdd'));
Route::get('app/generalInsurance/travel/view/{id}',array('uses'=>'TravelInsuranceAppController@travelInsuranceView'));
Route::post('app/generalInsurance/travel/edit/{details}',array('uses'=>'TravelInsuranceAppController@travelInsuranceEdit'));
Route::post('app/generalInsurance/travel/delete/{id}',array('uses'=>'TravelInsuranceAppController@travelInsuranceDelete'));

//Mutual Funds
Route::get('mutualFunds',array('before'=>'auth','uses'=>'MutualFundsController@mutualFunds'));

//Others
Route::get('otherInsurance',array('before'=>'auth','uses'=>'OthersController@others'));

//user profile 
Route::get('profile',array('before'=>'auth','uses'=>'ProfileController@viewProfile'));
Route::get('editProfile',array('before'=>'auth','uses'=>'ProfileController@editProfile'));
Route::post('editProfile',array('before'=>'auth','uses'=>'ProfileController@editProfilePost'));


//user app profile 
Route::get('app/profile/{details}',array('uses'=>'ProfileAppController@viewProfile'));
Route::post('app/editProfile/{details}',array('uses'=>'ProfileAppController@editProfilePost'));

//notification
Route::get('getNotification/{details}',array('uses' => 'NotificationAppController@getNotification'));
Route::get('getAllUnseenNotification/{details}',array('uses' => 'NotificationController@getAllunseenNotification'));
Route::get('dummy',array('uses' => 'NotificationAppController@notificationCreateMyInsurance'));


Route::get('allNotification',array('auth' => 'before','uses' => 'NotificationController@allNotification'));

//register a new user
Route::post('createUser/{userInfo}', 'UserAppController@createUser');
Route::get('userLogin/{userLoginInfo}', 'UserAppController@userLogin');
Route::get('loginWithFacebookByApp/{userDataFromFacebook}',array('as'=>'loginWithFacebookByApp','uses'=>'UserAppController@loginWithFacebookByApp'));
Route::get('loginWithGoogleByApp/{userDataFromGoogle}',array('as'=>'loginWithGoogleByApp','uses'=>'UserAppController@loginWithGoogleByApp'));
Route::post('registerGCMid/{details}','UserAppController@registerGCMid');
Route::post('setMpin/{details}','UserAppController@setMpin');
Route::post('validateMpin/{details}','UserAppController@validateMpin');
Route::post('resetMpin/{details}','UserAppController@resetMpin');
Route::get('resetMpin','UserAppController@resetMpinView');
Route::post('resetMpin','UserAppController@resetMpinViewPost');

Route::post('changeNotificationSettings/{details}','UserAppController@changeNotificationSettings');
Route::get('showAllUser', 'UserAppController@showAllUser');
Route::get('showNoti', 'UserAppController@showNoti');




//Life Insurance Web
Route :: get('pushnotification',array('uses'=>'NotificationAppController@sendNotification')); 
Route :: get('notification',array('uses'=>'NotificationAppController@sendEmailNotification')); 

 

Route :: get('select',function(){
	return View :: make('select');
});

Route ::get('noti',function(){
      $travelInsurance = InsuranceLife :: getTravelInsuranceRenewalInfo();
        if(count($travelInsurance) > 0){
            for($i=0;$i<count($travelInsurance);$i++){
                 $result=   Notification :: notificationCreateTravelInsurance($travelInsurance[$i]);
            }
            
        }
        return $result;
});